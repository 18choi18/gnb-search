# 이슈 타입을 정의한다

<A> : 스크립트 Lexical Scope 범위를 벗어난 식별자를 사용한다. (다른 것에 의존한다)
<B> : 스크립트에서 모듈로 Export 하려고 의도하지 않은 식별자(변수, 함수)가 전역변수로 선언된다. (다른 것이 이 파일에 의존할 수 있다)
<C> : 의도적으로 전역변수로 Export 하려고 한 객체(instance or function)로 해당 부분이 어디에 사용되었는지 파악한다.
<D> : DOM 에 해당 요소가 있다는 전제하에 로직이 진행되어 개편된 DOM 에서 실행 시 Cant't read property '...' of undefined 에러 발생.

## Dependency library modules

ga, jQuery, rakeLog, Handlebars

## Dependencing files

1. headerCommonJs.js
2. footerCommonJs.js
3. main.html
4. inc_footer_data.js

### HeaderGnb.search: function

1. <A> / chkKeywordEvent(keyword): function
   : footerCommonJS.js 2053 line

2. <A> / trim(gnbFormObj.kwd.value): function
   : headerCommonJs.js 3082 line

3. <A> / encodeKwdNew(keyword): function
   : headerCommonJs.js 2559 line

4. <D> / var gnbFormObj = document.forms["GNBSearchForm"];

5. <A> / ad_headerCommonJs: object
   : main.html 4493 line

### HeaderGnb.drawTemplate: function

6. <D> / var source = jQuery('#'+templateId).html();

### HeaderGnb.makeGnb(): function

7. <A> / funcCheckIsLogin: function
   : headerCommonJs.js 172 line

8. <A> 
   _ORDER_URL_: string
   _GNB_CONTEXT_PATH_: string
   _ORDER_HIS_URL_: string
   _RETURN_URL_: string
   _SELLER_URL_: string

   ... 너무 많음

: main.html 136 line...

9. <A> getBookMarkYn: function
   : headerCommonJs.js 5271 line

10. <A> FooterData: object
    : inc_footer_data.js

11. <B> randomIdx: number
    : 의도하지 않은 전역변수로 선언되었지만 main 기준으로 검색 결과 사용되지 않는 것으로 보임 ..

### HeaderGnb.categoryNavi_v2: object

### getCommonImgUrl: function

### getAltTxt: function
