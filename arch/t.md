# script 간 의존성 제거하여 헤더 에러 시 다른 페이지에 영향력 끊기

1. 1단? 2단? 모양, 윙, 푸터... 필요 모양을 기존에 받는 객체의 값으로 받는다.
2. 아래 제거 : script 에서 div 추가하고 pcComm.renderWing(); 실행시킨다.

---

<div id="wing">
    <script>pcComm.renderWing();</script>
</div>
---
