// not logined
DesktopLayout.render([
  {
    name: "Header",
    params: {
      type: "sub",
      user: { type: "0", name: "", grade: "Unknown" },
      isAdult: false
    },
    container: document.getElementById("header")
  }
]);

// logined
DesktopLayout.render([
  {
    name: "Header",
    params: {
      type: "sub",
      user: { type: "1", name: "최형석", grade: "SVIP" },
      isAdult: false
    },
    container: document.getElementById("header")
  }
]);
