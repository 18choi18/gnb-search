// <B> inc_header_v7.js 479 ... line
var gnbSeasonMenuBnr = { count: 0, items: [] };

// <B> inc_header_v7.js 314, 330 line
// 검색창 오른쪽 작은 배너 5개
var R_BANNER_LIST = [
  {
    imgUrl:
      "https://i.011st.com/browsing/banner/2020/05/08/29567/2020050814500813598_10709273_1.jpg",
    index: 1,
    title: "5월전사",
    weight: 20,
    regnCode: "MAINMGN0401",
    totalCount: 5,
    target: "",
    imgLink: "http://event.11st.co.kr/html/nc/event/20200501_movreview_p.html"
  },
  {
    imgUrl:
      "https://i.011st.com/browsing/banner/2020/04/23/29567/202004231705232875_10685003_1.jpg",
    index: 2,
    title: "5월 VIP쿠폰",
    weight: 20,
    regnCode: "MAINMGN0402",
    totalCount: 5,
    target: "",
    imgLink:
      "http://www.11st.co.kr/browsing/MallPlanDetail.tmall?method=getMallPlanDetail&planDisplayNumber=2032826"
  },
  {
    imgUrl:
      "https://i.011st.com/browsing/banner/2020/04/29/29567/2020042921312944850_10694096_1.jpg",
    index: 3,
    title: "AllPRIME",
    weight: 20,
    regnCode: "MAINMGN0403",
    totalCount: 5,
    target: "",
    imgLink:
      "http://www.11st.co.kr/browsing/MallPlanDetail.tmall?method=getMallPlanDetail&planDisplayNumber=2023429"
  },
  {
    imgUrl:
      "https://i.011st.com/browsing/banner/2020/04/24/29567/2020042418272419658_10686550_1.jpg",
    index: 4,
    title: "11번가신한카드",
    weight: 20,
    regnCode: "MAINMGN0404",
    totalCount: 5,
    target: "",
    imgLink:
      "http://www.11st.co.kr/browsing/MallPlanDetail.tmall?method=getMallPlanDetail&planDisplayNumber=2014938"
  },
  {
    imgUrl:
      "https://i.011st.com/browsing/banner/2020/05/15/29567/2020051521181535484_10726547_1.jpg",
    index: 5,
    title: "장바구니기획전",
    weight: 20,
    regnCode: "MAINMGN0405",
    totalCount: 5,
    target: "",
    imgLink:
      "http://www.11st.co.kr/browsing/MallPlanDetail.tmall?method=getMallPlanDetail&planDisplayNumber=2012466"
  }
];
var R_BANNER_SIZE = 5;
