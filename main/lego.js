var LEGO = {
	warpperObj : null,
	recommPrdCarrSn : 0,
	recommPrdBlckSn : 0,
	recommendIntervalObj : "",
	recommendPrdLength : 0,
	recommendPageIdx : 0,
	adAreaTopCarrSn : 0,
	adAreaBottomCarrSn : 0,
	hotCarrSn : 0,
	
	init : function(wrapperId){	

		Handlebars.registerHelper("mod", function(v1, initAdd, v2, v3, options) {
		    v1 = parseFloat(v1) + parseFloat(initAdd);
		    v2 = parseFloat(v2);
		    v3 = parseFloat(v3);
		    
		    if((v1 % v2) == v3){
		    	return options.fn(this);
		    }else{
		    	return options.inverse(this);
		    }
		});

		Handlebars.registerHelper('ifCond', function (v1, operator, v2, options) {
		    switch (operator) {
		        case '==':
		            return (v1 == v2) ? options.fn(this) : options.inverse(this);
		        case '!=':
		            return (v1 != v2) ? options.fn(this) : options.inverse(this);
		        case '===':
		            return (v1 === v2) ? options.fn(this) : options.inverse(this);
		        case '<':
		            return (parseInt(v1) < parseInt(v2)) ? options.fn(this) : options.inverse(this);
		        case '<=':
		            return (parseInt(v1) <= parseInt(v2)) ? options.fn(this) : options.inverse(this);
		        case '>':
		            return (parseInt(v1) > parseInt(v2)) ? options.fn(this) : options.inverse(this);
		        case '>=':
		            return (parseInt(v1) >= parseInt(v2)) ? options.fn(this) : options.inverse(this);
		        case '&&':
		            return (v1 && v2) ? options.fn(this) : options.inverse(this);
		        case '||':
		            return (v1 || v2) ? options.fn(this) : options.inverse(this);
		        default:
		            return options.inverse(this);
		    }
		});
		
		Handlebars.registerHelper('jsonToString', function (object) {
			return JSON.stringify(object);
		});
	},
	
	setWarpper : function(wrapperId){
		this.warpperObj = jQuery("#" + wrapperId);
	},
		
	getHandleBarsMarkup : function(template, jsonData){
		try{
			if(jQuery("#" + template)){
			    var source = jQuery("#" + template).html();
			    var template = Handlebars.compile(source);
			    return template(jsonData);
			}
			return '';
		}catch(e){
		}
	},
	
	constructing : function(carrierJsonArr){
		
		var _this = this;
		
		var markup = "";
		var markupTemplate = "";
		
		for(var carrierIdx = 0; carrierIdx < carrierJsonArr.length; carrierIdx++){
			var buffer = [];
			
			var carrierInfo = carrierJsonArr[carrierIdx];
			
			var blockList = carrierInfo.blockList;
			
			if(blockList != null && blockList.length > 0){
				var isDrawCarrier = true;
				if(carrierInfo.type == 'caption_carrier' && (blockList[0].list == null || blockList[0].list.length == 0)){
					isDrawCarrier = false;
				}
			
				if(isDrawCarrier){
					buffer.push(this.getHandleBarsMarkup("carrier_template_" + carrierInfo.type, carrierInfo));
					
					// Draw Carrier
					var carrierObj = this.warpperObj.append(buffer.join(''));		
					
					buffer = [];
					
					for(var blockIdx = 0; blockIdx < blockList.length; blockIdx++){
						var blockData = blockList[blockIdx];
						
						blockData["carrSn"] = carrierInfo.carrSn;
						blockData["blockIndex"] = blockIdx;
						blockData["carrTitle"] = carrierInfo.title1;
						
						// 초기 노출 블록 (TabCarrier)
						if(carrierInfo.type.indexOf("TabCarrier") > -1){
							
							try{
								blockData["moreLinkUrl"] = carrierInfo.tabList[blockIdx].tabLink;
								blockData["tabTitle"] = carrierInfo.tabList[blockIdx].tabTitle;
							}catch(e){}
							
							if(carrierInfo.initTab == blockIdx){
								blockData["blockVisible"] = "block";
							}else{
								blockData["blockVisible"] = "none";
							}
						}else{
							blockData["blockVisible"] = "block";
						}
						
						// 블록 서브 탭 초기 노출
						if(blockData.initSubTabIdx && blockData.initSubTabIdx != undefined){
							for(i = 0 ; i < blockData.initSubTabIdx.length; i++){
								if(blockData.initSubTabIdx[i] == -1){ // 랜덤 초기화
									blockData.initSubTabIdx[i] = this.getRandomDispSubTabIdx(blockData.list[i]);
								}
							}
						}

						// 블록 내 데이터 랜덤 노출 여부
						if(blockData.randomDisp && blockData.randomDisp != undefined){
							for(i = 0 ; i < blockData.randomDisp.length; i++){
								if(blockData.randomDisp[i] == "Y"){
									blockData.list[i] = this.getRandomDispSpceData(blockData.list[i], blockData.randomDispCnt[i]);
								}
							}
						}
						
						// 블록내 영역별 데이터 개수
						if(blockData.list && blockData.list != undefined){
							var blockDataSizeArr = [];
							for(i = 0; i < blockData.list.length; i++){
								blockDataSizeArr[i] = blockData.list[i].length;
							}
							blockData["dataSize"] = blockDataSizeArr;
						}
					
						if("B_04_RecommPrd" == blockData.type){
							this.recommPrdCarrSn = carrierInfo.carrSn;
							this.recommPrdBlckSn = blockData.blckSn;
						}else if("B_01_AD_Top" == blockData.type){
							this.adAreaTopCarrSn = carrierInfo.carrSn;
						}else if("B_01_AD_Bottom" == blockData.type){
							this.adAreaBottomCarrSn = carrierInfo.carrSn;
						}else if("B_04_Hot" == blockData.type){
							this.hotCarrSn = carrierInfo.carrSn;
							buffer.push(this.getHandleBarsMarkup("block_template_" + blockData.type, blockData));
						}else{
							buffer.push(this.getHandleBarsMarkup("block_template_" + blockData.type, blockData));
						}
					}
					
					// Draw Block
					jQuery("#carrier_" + carrierInfo.carrSn).append(buffer.join(''));				
				}
			}
		}
		
		jQuery(document).ready(function(){
			_this.adAreaTop();
			_this.adAreaBottom();
			Main.todayChance.init();
			_this.hot().init();
			
			jQuery4Lego("img.lazy").lazyload({
				threshold : 500
			});
			
			try{
				jQuery("#mainLegoArea a").each(function(idx, obj) {
					jQuery(obj).bind('click', catchAnchorGAToEvent);
				});
			}catch(e){}
			
		});
	},
	
	getRandomDispSpceData : function(dispSpceData, dispCnt){		
		var dispSpceDataArr = [];
		
		var randomDispCnt = 0;
		var randomIdxStr = "";
		
		while(randomDispCnt < dispCnt){
			var randomIdx = Math.floor(Math.random() * dispSpceData.length);
			if(randomIdxStr.indexOf("#" + randomIdx + "#") == -1){
				dispSpceDataArr[randomDispCnt] = dispSpceData[randomIdx];
				randomIdxStr += "#" + randomIdx + "#";
				randomDispCnt++;
			}
		}
		
		return dispSpceDataArr;
	},
	
	getRandomDispSubTabIdx : function(dispSpceData){
		var randomIdx = Math.floor(Math.random() * dispSpceData.length);
		
		return randomIdx;
	},
	
	displayBlock : function(carrSn, blockIndex){
		var carrDiv = jQuery("#carrier_" + carrSn);
		jQuery("div[divType=block]", carrDiv).hide();
		
		var blockDiv = jQuery("div[name=block_" + blockIndex + "]", carrDiv); 
		blockDiv.show();
		jQuery4Lego(blockDiv.find("img.lazy")).lazyload();
		
		var blockTab = jQuery("li[name=blockTab]", carrDiv); 
		blockTab.removeClass("on");
		jQuery(blockTab[blockIndex]).addClass("on");
	},
	
	displayBlockSubTab : function(carrSn, blckSn, subTabIndex){
		var blckDiv = jQuery("#block_" + carrSn + "_" + blckSn);
		var blockSubTabList = jQuery("[name=block_subTab]", blckDiv);
		var blockSubTabNav = jQuery("[name=blockSubTabNav]", blckDiv);
		
		blockSubTabList.hide();
		
		var blockSubTab = jQuery(blockSubTabList[subTabIndex]); 
		blockSubTab.show();
		jQuery4Lego(blockSubTab.find("img.lazy")).lazyload();
		
		blockSubTabNav.removeClass("on");
		jQuery(blockSubTabNav[subTabIndex]).addClass("on");
	},
	
	recommendPrd : function(){
		var _this = this;
		jQuery.ajax({
		    url : 'http://www.11st.co.kr/browsing/MainAjax.tmall?method=getRecopickUserInfoJson',
		    type : 'get',
		    dataType : 'json',
		    success : function (data) {
				
		    	var initSubTabIdxArr = [];
		    	initSubTabIdxArr[0] = Math.floor(Math.random() * data.prdList.length);;

		    	data["initSubTabIdx"] = initSubTabIdxArr;
		    	data["carrSn"] = _this.recommPrdCarrSn;
		    	data["blckSn"] = _this.recommPrdBlckSn;
		    	
		    	var buffer = [];
		    	buffer.push(_this.getHandleBarsMarkup("block_template_B_04_RecommPrd", data));
		    	
		    	var recommPrdCarrDiv = jQuery("#carrier_" + _this.recommPrdCarrSn); 
		    	recommPrdCarrDiv.append(buffer.join(''));
		    	jQuery4Lego(recommPrdCarrDiv.find("img.lazy")).lazyload();
		    	
		    	try{
					jQuery("a", recommPrdCarrDiv).each(function(idx, obj) {
						jQuery(obj).bind('click', catchAnchorGAToEvent);
					});
				}catch(e){}
				
				if(data.prdType == '0'){
					var prdEl = jQuery('ul.product_list_col_2', recommPrdCarrDiv).children();
					_this.recommendPrdLength = prdEl.length;
					
					if(_this.recommendPrdLength > 4){
						recommPrdCarrDiv.bind({
							mouseenter : function(){
								_this.recommendStopRolling();
							},
							mouseleave : function(){
								_this.recommendStartRolling();
							}
						});
						
						_this.recommendStartRolling();
					}
				}
		    }
		});
	},
	
	showRecommendPrd : function(pageIdx){
		var _this = this;
		var recommPrdCarrDiv = jQuery("#carrier_" + _this.recommPrdCarrSn);
		var prdEl = jQuery('ul.product_list_col_2', recommPrdCarrDiv).children();
		var pageButton = jQuery('div.bnr_exhibition_paging', recommPrdCarrDiv).children();
		
		_this.recommendPageIdx = pageIdx;
		
		var startIdx = _this.recommendPageIdx * 4;
		var endIdx = startIdx + 4;
		if(endIdx + 1 >= _this.recommendPrdLength){
			endIdx = _this.recommendPrdLength;
		}
		
		prdEl.hide();
		pageButton.removeClass("on");
		jQuery(pageButton[pageIdx]).addClass("on");
		for(var i = startIdx; i < endIdx; i++){
			jQuery(prdEl[i]).show();
		}
	},
	
	showNextRecommendPrd : function(){
		if(LEGO.recommendPageIdx + 1 == LEGO.recommendPrdLength / 4){
			LEGO.recommendPageIdx = 0;
		}else{
			LEGO.recommendPageIdx++;
		}
		
		LEGO.showRecommendPrd(LEGO.recommendPageIdx);
	},
	
	recommendStopRolling : function(){
		var _this = this;
		if(_this.recommendIntervalObj){
			clearInterval(_this.recommendIntervalObj);
			_this.recommendIntervalObj = "";
		}
	},
	
	recommendStartRolling : function(){
		var _this = this;
		if(_this.recommendPrdLength > 4 && !_this.recommendIntervalObj){
			_this.recommendIntervalObj = setInterval(_this.showNextRecommendPrd, 5000);
		}
	},
	
	adAreaTop : function(){
		jQuery.ajax({
			url : '//ds.11st.co.kr/NetInsight/text/11st/11st_main/main@New_Main_Right_cpt?callback=Ad_Main_Right',
			dataType : 'jsonp',
			jsonp : false,
			cache : false,
			jsonpCallback : 'Ad_Main_Right',
			success : function(data){}
		});	
	},

	adAreaBottom : function(){
		jQuery.ajax({
			url : '//www.11st.co.kr/wingBanner/WingBannerAjaxAction.tmall?method=getAdsServerUrl&adsArea=adAreaBottom',
			cache : false,
			success : function(data){
				var url = {url : data};
				Ad_Main_Bottom(url);
			}
		});
	},
	
	hot : function(){
		var $wrapper = jQuery("#carrier_" + this.hotCarrSn);
		var $personTabLiArr = jQuery('#box_person_hot .tab li', $wrapper);
		var $personContUlArr = jQuery('#box_person_hot .cont ul', $wrapper);
		var eventHandler = function(){
			$personTabLiArr.bind({
				mouseenter : function(evt){
					var $currObj = jQuery(evt.currentTarget);
					var selectedTabIdx = $personTabLiArr.index($currObj);
					jQuery($personContUlArr).hide();
					jQuery($personContUlArr[selectedTabIdx]).show();
					$personTabLiArr.removeClass("on");
					$currObj.addClass("on");
				}
			});
		};
		
		var setGATaging = function(){
			jQuery( document ).ready(function(){
				try {
					jQuery("a", $wrapper).each(function(idx, obj) {
						jQuery(obj).bind('click',catchAnchorGAToEvent);
					});
				} catch(e) {
				}
			});
		};
		
		var initialize = function(){
			eventHandler();
			setGATaging();
			
			//성별
			var genderCd = TMCookieUtil.getSubCookie(0, 'GND');
			if(genderCd == '10'){
				jQuery($personContUlArr).hide();
				jQuery($personContUlArr[0]).show();
				$personTabLiArr.removeClass("on");
				jQuery($personTabLiArr.get(0)).addClass("on");
			}
		};
		
		return {
			init : function(){
				initialize();
			}
		}
	}
};

function Ad_Main_Right(data){	
	data["carrSn"] = LEGO.adAreaTopCarrSn;
	
	var buffer = [];
	buffer.push(LEGO.getHandleBarsMarkup("block_template_B_01_AD_Top", data));
	
	var adMainRightDiv = jQuery("#carrier_" + LEGO.adAreaTopCarrSn); 
	adMainRightDiv.append(buffer.join(''));
}

function Ad_Main_Bottom(data){	
	data["carrSn"] = LEGO.adAreaBottomCarrSn;
	
	var buffer = [];
	buffer.push(LEGO.getHandleBarsMarkup("block_template_B_01_AD_Bottom", data));
	
	var adMainBottomDiv = jQuery("#carrier_" + LEGO.adAreaBottomCarrSn); 
	adMainBottomDiv.append(buffer.join(''));
}