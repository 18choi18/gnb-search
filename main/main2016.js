var Main = {};

//common (DataSet 포함)
Main.common = {
	currentData : {},
	currentDataList : {},
	totalCount : 0,
	showCount : 0,
	currentPosition : -1, //첫 데이터 idx
	isRandom : false,
	areaName : "",
	isDataLoad : false,
	_stckImg6 : [],
	_stckImg1 : [],

	init : function (dataName, boolRandom){
		var me = this;
		var randomIndex = 0;
		me.setData(dataName);
		me.isRandom = boolRandom;
		me.mainDataSet();
	},

	setData : function (dataName){
		var me = this;
		me.currentData = eval(dataName); //데이터 명
		me.currentDataList = me.currentData.DATA;
		me.totalCount = me.currentData.totalCount;
		me.showCount = me.currentData.showCount;
		me.areaName = dataName;
	},

	mainDataSet : function (){
		var me = this;
		var $IMG1, $URL1, $JURL1, $TXT1, $DSC1, $TRCNO1, $HTML1, $ETXT1;
		var strTRCNO = "";
		var startIdx = 1;
		var currPageNum = 0;
		var totalPageNum = 0;

		if( me.showCount > me.totalCount ) me.showCount = me.totalCount;
		if( me.isRandom ) {
			me.currentPosition =  Math.floor(Math.random()*Math.ceil(me.totalCount)) - 1;
		}
		for( i = startIdx; i <= me.showCount; i++) {
			me.currentPosition++;
			if( me.currentPosition >= me.totalCount ) me.currentPosition = 0;
			if( me.currentPosition < 0 ) me.currentPosition = me.totalCount - 1;

			try{
				$IMG1 	 = jQuery("#" + me.areaName + i + "_IMG1");
				$TXT1 	 = jQuery("#" + me.areaName + i + "_TXT1");
				$ETXT1 	 = jQuery("#" + me.areaName + i + "_ETXT1");
				$HTML1 	 = jQuery("#" + me.areaName + i + "_HTML1");
				$DSC1 	 = jQuery("#" + me.areaName + i + "_DSC1");
				$TRCNO1  = jQuery("#" + me.areaName + i + "_TRCNO1");
				$URL1 	 = jQuery("a[name=" + me.areaName + i + "_URL1]");
				$JURL1 	 = jQuery("a[name=" + me.areaName + i + "_JURL1]");
			}catch(e){}

			//Product Image Set
			if( $IMG1.length > 0 ) {
				$IMG1.attr("src", me.currentDataList[me.currentPosition].IMG1);
				if( $TXT1 ) {
					$IMG1.attr("alt", getAltTxt(me.currentDataList[me.currentPosition].ETXT1,me.currentDataList[me.currentPosition].TXT1));
				}
			}
			//Product Text Set
			if( $TXT1.length > 0 ) {
				$TXT1.html(me.currentDataList[me.currentPosition].TXT1);
			}
			//Normal Link Set
			if( $URL1.length > 0 ) {
				$URL1.each(function() {
					jQuery(this).attr("href", me.makeDataUrl(me.currentDataList[me.currentPosition]));
				})
			}
			//Javascript Link Set
			if( $JURL1.length > 0 ) {
				$JURL1.each(function() {
					jQuery(this).attr("href", me.makeDataUrl(me.currentDataList[me.currentPosition]));
				})
			}
			//Discount Rate Set
			if( $DSC1.length > 0 ) {
				$DSC1.html(me.currentDataList[me.currentPosition].DSC1);
			}
			//Html Area Set
			if( $HTML1.length > 0 ) {
				$HTML1.html(me.currentDataList[me.currentPosition].HTML1);
			}
			//Ad impression Code Title Set
			if( $TRCNO1.length > 0 ) {
				var trcGubn = $TRCNO1.attr("title");
				if(trcGubn != null & me.currentDataList[me.currentPosition].TRCNO1 > 0) {
					strTRCNO += trcGubn + me.currentDataList[me.currentPosition].TRCNO1 + "^";
				}
			}
		}
		//Impression
		if(strTRCNO != "") {
			var img = new Image();
			me._stckImg6.push(img);
			img.src = '//st.11st.co.kr/a.st?a='+strTRCNO;
		}
		me.isDataLoad = true;
	},
	makeDataUrl : function(data){
		var returnURL = "";
		try{
			if( data.JURL1 != undefined ) {
				returnURL = "javascript:" + data.JURL1;
			} else if( data.URL1 != undefined)  {
				returnURL = data.URL1;
			}
		}catch(e){}
		return returnURL;
	},
	goNotc : function(ntecNo,searchGubun){
		goCommonUrl("http://help.11st.co.kr/11st/bbs/Bbs.jsp?ntceNo="+ntecNo+"&searchGubun="+searchGubun);
	},
	setRandomBnr : function(){
		var me = this;
		me.currentPosition =  Math.floor(Math.random()*Math.ceil(me.totalCount));
		var $setBnr = jQuery('#SpecialService_BNR' + me.currentPosition);
		$setBnr.addClass("selected");
	},
	setBnrEventHandler : function (){
		var $layerBnrObjArr = jQuery('li', '.special_service');
		$layerBnrObjArr.live({
			click : function(thisObj){
				var $target = jQuery(thisObj.currentTarget);
				$layerBnrObjArr.removeClass('selected');
				$target.addClass("selected");
			}
		});
	},
	clickCPC : function(typGubn,areaGubn,trcNo){
		var me = this;
		var img2 = new Image();
		me._stckImg1.push(img2);
		img2.src = 'http://aces.11st.co.kr/cpc.do?c='+trcNo+'^'+typGubn+'^'+areaGubn+'&noCache='+new Date().getTime();
	},

	shockingDealPrdChk : function(prdTypCd){
		var chkType = false;
		var arrPrdType = ['09','10','14','19','20','25']; //무형상품 상품 타입 코드

		for ( var idxPrdTypCd = 0 ; idxPrdTypCd < arrPrdType.length; idxPrdTypCd++ ) {
			if ( arrPrdType[idxPrdTypCd] == prdTypCd ) {
				chkType = true;
				break;
			}
		}
		return chkType;
	}
};

Main.topDeal = {
	dealData : {
		id : 'shockingDeal',
		title : '쇼킹딜',
		curIdx : 0,
		totalCnt  : 0,
		dataList : null,
		defaultUrl : _SHOCKING_DEAL_URL_ + '/html/nc/deal/main.html',
		clickCd : 'MAINMB201',
		limitCnt : 11,	// 최대 데이터 수
		viewCnt : 11	// 노출 데이터 수
	},

	getDeal : function(id) {
		return this.dealData;
	},
	getClickStat : function(url, clickCd) {
		return "javascript:goStatUrl('"+ url +"', '" + clickCd + "');";
	},
	getUrl : function(prdNo, defaultUrl, url) {
		if ( typeof(url) == 'string' && url != '' ) {
			return url;
		} else {
			return defaultUrl + ( defaultUrl.indexOf('?') > -1 ? '&' : '?' ) + 'prdNo=' + prdNo;
		}
	},
	getRandomIdx : function(maxNo) {
		return Number(Math.round(Math.random()*(maxNo-1)));
	},
	getPrdHtml : function(data, prdClickCd) {
		var _html = '';
		var _defaultUrl =  this.dealData.defaultUrl;
		_html += '	<a href="' + this.getClickStat(this.getUrl(data.prdNo, _defaultUrl, data.url), prdClickCd) + '">';

		if(Main.common.shockingDealPrdChk(data.prdTypCd)){
			if(data.autoRfndYn == 'Y'){
				_html += '	<div class="billflag_wrap"><em class="bf_shipping">자동<br>환불<\/em><\/div>';
			}
		}else if ( data.freeDlv == 'Y' ) {
			_html += '		<div class="billflag_wrap"><em class="bf_shipping">무료<br>배송<\/em><\/div>';
		}

		if ( data.img ) {
			_html += '			<img src="' + _UPLOAD_URL_ + data.img + '" alt="' + data.prdNm + '" width="208px" height="167px">';
		}
		if ( Number(data.dscRt) > 5 ) {
			_html += '				<em>' + data.dscRt + '<span>%<\/span><\/em>';
			_html += '				<span>' + data.salePrice + '원'+ (data.opt == 'Y' ? '~' : '') +'<\/span>';
		} else {
			_html += '				<em class="special_price">특별가<\/span><\/em>';;
			_html += '				<span>' + data.salePrice + '원'+ (data.opt == 'Y' ? '~' : '') +'<\/span>';
		}
		_html += '	<\/a>';

		return _html;
	},
	setNaviHtml : function(id) {
		var _deal = this.getDeal(id);
		var _totalCnt = _deal.totalCnt;
		var _viewCnt = _deal.viewCnt;
		var _curIdx = _deal.curIdx;
		var $naviWrapper = jQuery('#' + id + '_navi_wrap');

		var _naviHtml = '';
		if ( _totalCnt > 1 ) {
			_naviHtml += '	<span><em id="' + id +'_curIdx">' + (_curIdx + 1) + '<\/em>/' + _viewCnt+'<\/span>';
			_naviHtml += '	<button type="button" class="in_prev" onclick="Main.topDeal.forward(\'' + id + '\')">이전 상품<\/button>';
			_naviHtml += '	<button type="button" class="in_next" onclick="Main.topDeal.next(\'' + id + '\')">다음 상품<\/button>';
		}
		$naviWrapper.html(_naviHtml);
	},
	setEvent : function(id) {
		var $wrapper = jQuery('#' + id + '_wrap');
		$wrapper.bind({
			mouseenter : function(evt){
				$wrapper.addClass('selected').siblings().removeClass('selected');
			},
			focusin : function(evt){
				$wrapper.addClass('selected').siblings().removeClass('selected');
			}
		});
	},
	setBaseHtml : function(id) {
		var _deal = this.getDeal(id);
		var _dataList = _deal.dataList;
		var _totalCnt = _deal.totalCnt;
		var _viewCnt = _deal.viewCnt;
		var _defaultUrl = _deal.defaultUrl;
		var _clickCd = _deal.clickCd;
		var _curIdx = _deal.curIdx;
		var $wrapper = jQuery('#' + id + '_wrap');

		var _html = '';
		_html += '<h1><a href="' + this.getClickStat(_defaultUrl, _clickCd+'01') + '">' + _deal.title + '<\/a><\/h1>';
		_html += '<div class="viewport">';
		_html += '	<ul id="' + id + '_li_wrap">';
		if ( _totalCnt > 0 ) {
			for ( var idx = 0 ; idx < _viewCnt; idx++ ) {
				if ( idx == _curIdx ) {
					_html += '<li>';
					_html += this.getPrdHtml(_dataList[idx], _clickCd + '02');
					_html += '<\/li>';
				} else {
					_html += '<li style="display:none"><\/li>';
				}
			}
		}
		_html += '	<\/ul>';
		_html += '<\/div>';
		_html += '<div class="btnctr_pn" id="' + id + '_navi_wrap"><\/div>';
		$wrapper.html(_html);
		this.setNaviHtml(id);
	},
	move : function(id, direction) {
		var $liObjList = jQuery('#' + id + '_li_wrap > li');
		var $tgtLiObj;
		var $curIdxObj = jQuery('#' + id + '_curIdx');

		var _deal = this.getDeal(id);
		var _curIdx = _deal.curIdx;
		var _viewCnt = _deal.viewCnt;
		var _clickCd = _deal.clickCd;

		// step1. 대상 Object 확인
		if ( direction == 'forward' ) {
			_curIdx = ( _viewCnt + ( _curIdx - 1) ) % _viewCnt;
			doCommonStat(_clickCd + '03');
		} else {
			_curIdx = ( _curIdx + 1 ) % _viewCnt;
			doCommonStat(_clickCd + '04');
		}
		_deal.curIdx = _curIdx;
		$tgtLiObj = jQuery($liObjList[_curIdx]);

		// step2. 대상 Object 내용 확인
		if ( $tgtLiObj.html() == '' ) {
			$tgtLiObj.html( this.getPrdHtml(_deal.dataList[_curIdx], _clickCd + '03') );
		}

		$liObjList.hide();
		$tgtLiObj.show();
		$curIdxObj.html(_curIdx + 1);
	},
	forward : function(id) {
		this.move(id, 'forward');
	},
	next : function(id) {
		this.move(id, 'next');
	},
	initDeal : function(id, jsObj) {
		var n = new Date();
		ndate = Date.UTC(n.getFullYear(), n.getMonth(), n.getDate(), n.getHours(), n.getMinutes(), 0);
		var s, sdate;
		var e, edate;

		try {
			var _deal = this.getDeal(id);
			var _dataList = jsObj.DATA;
			var _jsonArray = new Array();
			var _limitCnt = _deal.limitCnt;

			if ( _dataList ) {
				var _totalCnt  = _dataList.length;

				for (var _idx = 0 ; _idx < _totalCnt ; _idx++ ){
					s = _dataList[_idx].bgnDt;
		  			e = _dataList[_idx].endDt;
		  			sdate = Date.UTC(s.substr(0,4), Number(s.substr(4,2))-1, s.substr(6,2), s.substr(8,2), s.substr(10,2), 0);
		  			edate = Date.UTC(e.substr(0,4), Number(e.substr(4,2))-1, e.substr(6,2), e.substr(8,2), e.substr(10,2), 0);

		  			if( (ndate-sdate) >= 0 && (ndate-edate) <= 0 ) {
		  				var _data = _dataList[_idx];

		  				var tmpData = {
		  						dealId : id,
		  						prdNo : _data.prdNo,
		  						prdNm : _data.prdNm,
		  						img : _data.img,
		  						url : _data.url,
		  						price : _data.price,
		  						salePrice : _data.salePrice,
		  						opt : _data.opt,
		  						dscRt : _data.dscRt,
		  						freeDlv : _data.freeDlv,
		  						soldOut : _data.soldOut,
		  						autoRfndYn: _data.autoRfndYn,
		  						prdTypCd: _data.prdTypCd
		  				}
		  				_jsonArray.push(tmpData);

			  			if(_idx + 1 == _limitCnt) {
			  				break;
			  			}
		  			}
				}
				_deal.dataList = _jsonArray;
				_deal.totalCnt = _jsonArray.length;
				if ( _deal.viewCnt > _deal.totalCnt ) {
					_deal.viewCnt = _deal.totalCnt;
				}
				var _rndIdx = this.getRandomIdx(_deal.viewCnt);
				_deal.curIdx = _rndIdx;
			}
		} catch (ex) { }

		this.setBaseHtml(id);
		this.setEvent(id);
	},
	focusTab : function(id) {
		try {
			jQuery('#' + id + '_wrap').addClass('selected');
		} catch (ex) {}
	},
	init : function() {
		var _id = 'shockingDeal';
		var _rndIdx = this.getRandomIdx(1);
		this.initDeal(_id, ShockingDealList);
		this.focusTab(_id);
	}
};
Main.oneDay = {
	dealData : {
		id : 'oneDay',
		title : 'oneDay',
		curIdx : 0,
		totalCnt  : 0,
		dataList : null,
		clickCd : 'MAINMB3',
		limitCnt : 12,	// 최대 데이터 수
		viewCnt : 12,	// 노출 데이터 수
		dayIndex : 0	// 요일 인덱스 0은 월요일, 6은 일요일
	},
	getDeal : function(id) {
		return this.dealData;
	},
	getClickStat : function(url, clickCd) {
		return "javascript:goStatUrl('"+ url +"', '" + clickCd + "');";
	},
	getUrl : function(prdNo, url) {
		if ( typeof(url) == 'string' && url != '' ) {
			return url;
		}
	},
	getRandomIdx : function(maxNo) {
		return Number(Math.round(Math.random()*(maxNo-1)));
	},
	getPrdHtml : function(data, prdClickCd) {
		var _html = '';
		_html += '	<a href="' + this.getClickStat(data.url, prdClickCd) + '">';

		if ( data.freeDlv == 'Y' ) {
			_html += '		<div class="billflag_wrap"><em class="bf_shipping">무료<br>배송<\/em><\/div>';
		}

		if ( data.img ) {
			_html += '		<img src="' + _UPLOAD_URL_ + data.img + '" alt="' + data.prdNm + '" width="208px" height="167px">';
		}
		_html += '			<span>' + data.price + '원'+ (data.optYn == 'Y' ? '~' : '') +'<\/span>';
		_html += '	<\/a>';

		return _html;
	},
	setNaviHtml : function(id) {
		var _deal = this.getDeal(id);
		var _totalCnt = _deal.totalCnt;
		var _viewCnt = _deal.viewCnt;
		var _curIdx = _deal.curIdx;
		var $naviWrapper = jQuery('#' + id + '_navi_wrap');

		var _naviHtml = '';
		if ( _totalCnt > 1 ) {
			_naviHtml += '	<span><em id="' + id +'_curIdx">' + (_curIdx + 1) + '<\/em>/' + _viewCnt+'<\/span>';
			_naviHtml += '	<button type="button" class="in_prev" onclick="Main.oneDay.forward(\'' + id + '\')">이전 상품<\/button>';
			_naviHtml += '	<button type="button" class="in_next" onclick="Main.oneDay.next(\'' + id + '\')">다음 상품<\/button>';
		}
		$naviWrapper.html(_naviHtml);
	},
	setEvent : function(id) {
		var $wrapper = jQuery('#' + id + '_wrap');
		$wrapper.bind({
			mouseenter : function(evt){
				$wrapper.addClass('selected').siblings().removeClass('selected');
			},
			focusin : function(evt){
				$wrapper.addClass('selected').siblings().removeClass('selected');
			}
		});
		
		/*

		jQuery('#oneDayTitle').bind({
			mouseenter : function(evt){
				Main.oneDay.showLayer();
				jQuery('#oneDayLayerWrap').addClass('selected').siblings().removeClass('selected');

			},
			focusin : function(evt){
				jQuery('#oneDayLayerWrap').addClass('selected').siblings().removeClass('selected');
				Main.oneDay.showLayer();
			},
			mouseleave : function(evt){
				jQuery('#oneDayLayerWrap').removeClass('selected');
			}
		});

		jQuery('.tit_banner').bind({
			mouseleave : function(evt){
				jQuery('#oneDayLayerWrap').removeClass('selected');
			}
		});

		var $layerWrapper = jQuery('#oneDayLayerWrap');

		$layerWrapper.bind({
			mouseleave : function(evt){
				jQuery('#oneDayLayerWrap').removeClass('selected');
			},
			mouseenter : function(evt){
				jQuery('#oneDayLayerWrap').addClass('selected');
			}
		});

		jQuery('li', $layerWrapper).bind({
			mouseenter : function(evt){
				var $target = jQuery(evt.currentTarget);
				var index = jQuery('li', $layerWrapper).index($target);
				Main.oneDay.showLayer(index);
			},
			focusin : function(evt){
				var $target = jQuery(evt.currentTarget);
				var index = jQuery('li', $layerWrapper).index($target);
				Main.oneDay.showLayer(index);
			}
		});

		jQuery('[name=btn_close]', $layerWrapper).bind({
			click : function(evt){
				jQuery('#oneDayLayerWrap').removeClass('selected');
			},
			focusout : function(evt){
				jQuery('#oneDayLayerWrap').removeClass('selected');
			}
		});
		*/
	},
	setBaseHtml : function(id) {
		var _deal = this.getDeal(id);
		var _dataList = _deal.dataList;
		var _totalCnt = _deal.totalCnt;
		var _viewCnt = _deal.viewCnt;
		var _clickCd = _deal.clickCd;
		var _curIdx = _deal.curIdx;

		var _curTitleDataList = null;
		var _curTitleData = null;

		if(typeof(OneDayTitleList) != 'undefined'){
			_curTitleDataList = OneDayTitleList.DATA;
			if(_curTitleDataList.length > 0){
				for(var titleIdx = 0; titleIdx < _curTitleDataList.length; titleIdx ++){
					var s = _curTitleDataList[titleIdx].bgnDt;
		  			var e = _curTitleDataList[titleIdx].endDt;
		  			var sdate = Date.UTC(s.substr(0,4), Number(s.substr(4,2))-1, s.substr(6,2), s.substr(8,2), s.substr(10,2), 0);
		  			var edate = Date.UTC(e.substr(0,4), Number(e.substr(4,2))-1, e.substr(6,2), e.substr(8,2), e.substr(10,2), 0);
		  			if( (ndate-sdate) >= 0 && (ndate-edate) <= 0 ) {
		  				_curTitleData = _curTitleDataList[titleIdx];
		  				break;
		  			}
				}
			}
		}

		var $wrapper = jQuery('#' + id + '_wrap');
		var _html = '';
		if(_curTitleData && _curTitleData.IMG1){
			_html += '<h1 class="tit_banner">';
			_html += '<a id="oneDayTitle" href="' + _curTitleData.URL1 + '" onclick="doCommonStat(\'MAINMB30201\');"><img src="' + _curTitleData.IMG1 + '" alt="' + _curTitleData.TXT1 + '" width="120px" height="29px"><\/a>';
			_html += '<\/h1>';
			if(_curTitleData.IMG2){
				_html += '<span class="benefit"><img src="' + _curTitleData.IMG2 + '" alt="' + _curTitleData.TXT1 + '" width="35px" height="31px"></span>';
			}
		}
		_html += '<div class="viewport">';
		_html += '	<ul id="' + id + '_li_wrap">';
		if ( _totalCnt > 0 ) {
			for ( var idx = 0 ; idx < _viewCnt; idx++ ) {
				if ( idx == _curIdx ) {
					_html += '<li>';
					_html += this.getPrdHtml(_dataList[idx], _clickCd + '0202');
					_html += '<\/li>';
				} else {
					_html += '<li style="display:none"><\/li>';
				}
			}
		}
		_html += '	<\/ul>';
		_html += '<\/div>';
		_html += '<div class="btnctr_pn" id="' + id + '_navi_wrap"><\/div>';
		$wrapper.html(_html);
		this.setNaviHtml(id);
	},
	showLayer : function(index){
		/*
		if(index == undefined){
			index = this.dealData.dayIndex;
		}

		var $wrapper = jQuery('#oneDayLayerWrap');

		try{
			var $target = jQuery(jQuery('li', $wrapper).get(index));
			jQuery('li', $wrapper).removeClass('selected');
			$target.addClass('selected');
			if(jQuery('a[name=link]', $target).attr('href') == '#'){
				var bnrData = OneDayLayerList.DATA[index];
				jQuery('img[name=image]', $target).attr('src', bnrData.IMG1);
				jQuery('img[name=image]', $target).attr('alt', bnrData.TXT1);
				jQuery('a[name=link]', $target).attr('href', bnrData.URL1);
			}
		}catch(e){
			$wrapper.removeClass('selected');
		}
		*/
	},
	move : function(id, direction) {
		var $liObjList = jQuery('#' + id + '_li_wrap > li');
		var $tgtLiObj;
		var $curIdxObj = jQuery('#' + id + '_curIdx');

		var _deal = this.getDeal(id);
		var _curIdx = _deal.curIdx;
		var _viewCnt = _deal.viewCnt;
		var _clickCd = _deal.clickCd;

		// step1. 대상 Object 확인
		if ( direction == 'forward' ) {
			_curIdx = ( _viewCnt + ( _curIdx - 1) ) % _viewCnt;
			doCommonStat(_clickCd + '0203');
		} else {
			_curIdx = ( _curIdx + 1 ) % _viewCnt;
			doCommonStat(_clickCd + '0204');
		}
		_deal.curIdx = _curIdx;
		$tgtLiObj = jQuery($liObjList[_curIdx]);

		// step2. 대상 Object 내용 확인
		if ( $tgtLiObj.html() == '' ) {
			$tgtLiObj.html( this.getPrdHtml(_deal.dataList[_curIdx], _clickCd + '0202') );
		}

		$liObjList.hide();
		$tgtLiObj.show();
		$curIdxObj.html(_curIdx + 1);
	},
	forward : function(id) {
		this.move(id, 'forward');
	},
	next : function(id) {
		this.move(id, 'next');
	},
	initialize : function(id, jsObj) {
		var n = new Date();
		ndate = Date.UTC(n.getFullYear(), n.getMonth(), n.getDate(), n.getHours(), n.getMinutes(), 0);
		var s, sdate;
		var e, edate;
		try {
			var _deal = this.getDeal(id);
			var _dataList = jsObj.DATA;
			var _jsonArray = new Array();
			var _limitCnt = _deal.limitCnt;

			if ( _dataList ) {
				var _totalCnt  = _dataList.length;
				for (var _idx = 0 ; _idx < _totalCnt ; _idx++ ){
					s = _dataList[_idx].bgnDt;
		  			e = _dataList[_idx].endDt;
		  			sdate = Date.UTC(s.substr(0,4), Number(s.substr(4,2))-1, s.substr(6,2), s.substr(8,2), s.substr(10,2), 0);
		  			edate = Date.UTC(e.substr(0,4), Number(e.substr(4,2))-1, e.substr(6,2), e.substr(8,2), e.substr(10,2), 0);
		  			if( (ndate-sdate) >= 0 && (ndate-edate) <= 0 ) {
		  				var _data = _dataList[_idx];
		  				var tmpData = {
		  						prdNo : _data.NUM1,
		  						prdNm : _data.TXT1,
		  						img : _data.IMG2,
		  						url : _data.URL1,
		  						price : _data.PRC1,
		  						freeDlv : _data.FREE_DLV,
		  						optYn : _data.OPT_YN
		  				}
		  				_jsonArray.push(tmpData);
			  			if(_idx + 1 == _limitCnt) {
			  				break;
			  			}
		  			}
				}
				_deal.dataList = _jsonArray;
				_deal.totalCnt = _jsonArray.length;
				if ( _deal.viewCnt > _deal.totalCnt ) {
					_deal.viewCnt = _deal.totalCnt;
				}
				var _rndIdx = this.getRandomIdx(_deal.viewCnt);
				_deal.curIdx = _rndIdx;
			}
		} catch (ex) {}

		this.setBaseHtml(id);
		this.setEvent(id);
		//this.setDayIndex();
	},
	setDayIndex : function(){
		/*
		//요일 구하기
		var d = new Date();
		var currentday = d.getDay();
		var index = currentday - 1;
		if(index < 0){
			index = jQuery('li', '#oneDayLayerWrap').size() - 1;
		}
		this.dealData.dayIndex = index;
		*/
	},
	init : function() {
		var _rndIdx = this.getRandomIdx(1);
		if(typeof(OneDayPrdList) != 'undefined'){
			this.initialize(this.dealData.id, OneDayPrdList);
		}
	}
};

//메인 하단 카드혜택
Main.cardArea = (function(){
	var $wrapper = {};
	var data = {};
	var dataSize = 0;
	var startIdx = 0;
	var $bnrObjArr = {};
	var $nextBtnObj = {};
	var $prevBtnObj = {};

	var setData = function(){
		data = eval("CardBenefit");
		
		if( data && data.totalCount ){
			dataSize = data.totalCount;
			
			if(dataSize < 3){
				$nextBtnObj.hide();
				$prevBtnObj.hide();
			}
		}else{
			jQuery("#cardBenefitWrap").hide();
		}
		
		
	};
	var setWrapper = function(){
		$wrapper = jQuery("#cardBenefitWrap");
	};
	var setBnrObj = function(){
		$bnrObjArr = jQuery("li[name=cardBnr]", $wrapper);
	};
	var setBtnObj = function(){
		$nextBtnObj = jQuery("button[name=cardNext]", $wrapper);
		$prevBtnObj = jQuery("button[name=cardPrev]", $wrapper);
	};
	var setStartIdx = function(){
		startIdx = getRandom(dataSize);
	};
	var getRandom = function(size){
		return Math.round(Math.random() * (size - 1));
	};
	var initialize = function(){
		setWrapper();
		setBnrObj();
		setBtnObj();
		setData();
		setStartIdx();
		drawBanner();
		eventHandler();
	};

	var drawBanner = function(){
		try{
			for(var i=0; i<$bnrObjArr.length; i++){
				var currIdx = startIdx + i;
				if(currIdx < 0){
					currIdx = dataSize - 1;
				}else if(currIdx == dataSize){
					currIdx = 0;
				}else if(currIdx > dataSize){
					currIdx = 1;
				}
				var currData = data.DATA[currIdx];
				jQuery("img[name=cardImage]", $bnrObjArr[i]).attr({"src": currData.IMG1,"alt" : getAltTxt(currData.ETXT1,currData.TXT1)});
				jQuery("a[name=cardLink]", $bnrObjArr[i]).attr("href", currData.URL1);
			}
		}catch(e){
		}
	}

	var eventHandler = function(){
		$nextBtnObj.bind({
			click : function(){
				startIdx = startIdx + 1;

				if(startIdx > dataSize){
					startIdx = 1;
				}else if(startIdx == dataSize){
					startIdx = 0;
				}
				drawBanner();
			}
		});

		$prevBtnObj.bind({
			click : function(){
				startIdx = startIdx - 1;

				if(startIdx < 0){
					startIdx = dataSize-1;
				}else if(startIdx == 0){
					startIdx = dataSize;
				}
				drawBanner();
			}
		});
	};

	return {
		init : function(){
			initialize();
		}
	}
})();


//물성 카테고리 영역
Main.brandCategory = (function(){
	var $wrapper = {};
	var data = {};
	var startIdx = 0;
	var dataSize = 0;
	var $nextBtnObj = {};
	var $prevBtnObj = {};
	var $bnrObjArr = {};
	var isMove = false;
	var speed = 0.7;
	var viewSize = 0;
	var dataSize = 0;
	var _clickCd = "MAINMB8010";

	var setWrapper = function(){
		$wrapper = jQuery("#brandCateWrap");
	};

	var setBtnObj = function(){
		$nextBtnObj = jQuery('button.in_next', '.bnr_wrap');
		$prevBtnObj = jQuery('button.in_prev', '.bnr_wrap');
	};

	var setBnrObj = function(){
		$bnrObjArr = jQuery('.viewport li', '.bnr_wrap');
	};

	var setData = function(){
		data = eval("BrandFashion");
		if( typeof(data) == "object" ) {
			viewSize = data.showCount;
			dataSize = data.totalCount;

			if(dataSize != 8){
				$nextBtnObj.hide();
				$prevBtnObj.hide();
			}
		}else{
			jQuery("div[name=divBnrCate]").hide();
		}
	};

	var setStartIdx = function(){
		startIdx = getRandom(dataSize);
	};
	var getRandom = function(size){
		return Math.round(Math.random() * (dataSize - 1));
	};

	var drawBanner = function(){
		if (dataSize < 4){
			dataSize = 0;
		}else if ( 4 < dataSize && dataSize< 8){
			dataSize = 4;
		}
		if (dataSize > 0){
			var currIdx = startIdx;
			for(var i=startIdx; i<dataSize+startIdx; i++){
				if(currIdx == dataSize){
					currIdx = 0;
				}else if(currIdx > dataSize){
					currIdx = 1;
				}
				var currData = data.DATA[currIdx];
				$wrapper.append('<li><a href="javascript:goStatUrl(\''+currData.URL1+'\', \''+_clickCd+(i+1)+'\')";><img src="'+currData.IMG1+'" alt="'+currData.TXT1+'" /><\/a><\/li>');
				currIdx++;
			}
			$(".bnr_wrap li:nth-child(4n)").addClass("last_child");
			setBnrObj();
			$wrapper.css({'width': $bnrObjArr.outerWidth(true) * dataSize});
			if ($prevBtnObj){
				dataSize > viewSize ? setBtnHandler(true) : setBtnHandler(false);
			}
		}
	}

	var setBtnHandler = function(flag){
		if (flag){
			$prevBtnObj.bind({
				click : function(e){
					if (!isMove){
						doCommonStat('MAINMB80301');
						e.preventDefault();
						move(-1);
					}
				}
			});
			$nextBtnObj.bind({
				click : function(e){
					if (!isMove){
						doCommonStat('MAINMB80302');
						e.preventDefault();
						move(1);
					}
				}
			});
		} else {
			$prevBtnObj.remove();
			$nextBtnObj.remove();
		}
	};

	var move = function(dir) {
		isMove = true;
		if (dir > 0){
			TweenMax.to($wrapper, speed, {marginLeft:-$bnrObjArr.outerWidth(true) * viewSize, ease:Cubic.easeInOut, onComplete:function(){
				$wrapper.css({'marginLeft':0});
				for (var i = 0; i < viewSize; ++i){ $wrapper.append($bnrObjArr.filter(':first-child')); }
				isMove = false;
			}});
		} else {
			for (var i = 0; i < viewSize; ++i){ $wrapper.prepend($bnrObjArr.filter(':last-child')) }
			TweenMax.fromTo($wrapper, speed, {marginLeft:-$bnrObjArr.outerWidth(true) * viewSize}, {marginLeft:0, ease:Cubic.easeInOut, onComplete:function(){
				$wrapper.css({'marginLeft':0});
				isMove = false;
			}});
		}
	};

	var initialize = function(){
		setWrapper();
		setBtnObj();
		setData();
		setStartIdx();
		drawBanner();
	};

	return{
		init:function(){
			initialize();
		}
	}
})();

Main.footer = {
	//하단 수상내역
	MainAwardArea : function(){
		if(typeof(mainAwardHtml) != 'undefined' && mainAwardHtml != '' && mainAwardHtml.html1.length > 0) {
			return "<div class='award_list'>" + mainAwardHtml.html1 + "<\/div>";
		}
	}
}

//백화점/브랜드 템플릿 영역
Main.deppartmentBrand = {
	data : {
		pageCntNum		: -1,
		pageTotNum		: 2,
		isMove			: false,
		root			: "",
		lists			: "",
		gap				: 980,
		speed			: 0.7,
		intervalObj 	: null
	},
	init : function(){
		this.data.root = $('#tmpWrap');
		this.data.lists = $('#tmpWrap > div');

		var _this = this;
		this.data.root.addClass('loadscript');
		this.data.pageTotNum = this.data.lists.length;
		this.data.lists.each(function(idx){
			var pos = _this.data.gap;
			$(this).css({'left' : pos});
		});

		if(tmpltCnt > 1){
			jQuery("#tmpWrap > button").show();
		}

		var initIdx = Math.floor(Math.random()*Math.ceil(this.data.pageTotNum)) - 1;

		this.data.pageCntNum = initIdx;

		this.setBtnHandler();

		this.move(-1);

		if(this.data.lists.length > 1){
			this.startRolling();
		}
	},

	setBtnHandler : function(){
		var _this = this;

		this.data.root.bind({
			mouseenter : function(evt){
				_this.stopRolling();
			},
			mouseleave : function(evt){
				_this.startRolling();
				trace("mouseleave");
			},
			focusin : function(evt){
				_this.stopRolling();
			},
			focusout : function(evt){
				_this.startRolling();
			}
		});

		this.data.root.find('button.in_prev').bind({
			click : function(e){
				doCommonStat("MAINMB70401");

				if (!_this.data.isMove)
				{
					e.preventDefault();
					_this.move(1);
				}
			}
		});
		this.data.root.find('button.in_next').bind({
			click : function(e){
				doCommonStat("MAINMB70402");

				if (!_this.data.isMove)
				{
					e.preventDefault();
					_this.move(-1);
				}
			}
		});
	},
	move : function(dir) {
		this.data.isMove = true;
		var _this = this;

		if (_this.data.pageCntNum > -1)
		{
			var $oldTarget = this.data.lists.eq(this.data.pageCntNum);
			TweenMax.to($oldTarget, this.data.speed, {left:_this.data.gap * dir, ease:Cubic.easeInOut});
		}
		if (dir > 0)
		{
			this.data.pageCntNum = this.data.pageCntNum == 0 ? this.data.pageTotNum - 1 : this.data.pageCntNum - 1;
		} else {
			this.data.pageCntNum = this.data.pageCntNum == this.data.pageTotNum - 1 ? 0 : this.data.pageCntNum + 1;
		}

		var $target = this.data.lists.eq(this.data.pageCntNum);

		var $linkArr = jQuery("a", $target);
		var $imageArr = jQuery("img", $target);

		if(jQuery($linkArr[0]).attr("href") == "#"){
			if($target.hasClass("temptype_a")){
				var targetData = eval("dptBrdData" + (this.data.pageCntNum + 1));
				if(targetData.DATA.length > 0){
					if(targetData.DATA[0].TXT2 != ""){
						$target.css("background", targetData.DATA[0].TXT2);
					}
				}
			}

			this.drawTmpltData($linkArr, $imageArr, "dptBrdData" + (this.data.pageCntNum + 1));
		}

		$target.css('left', dir * -this.data.gap);
		TweenMax.to($target, this.data.speed, {left:0, ease:Cubic.easeInOut, onComplete:function(){_this.data.isMove = false;}});
	},
	drawTmpltData : function(linkArr, imageArr, dataId){

		var _this = this;

		var targetData = eval(dataId);

		jQuery(targetData.DATA).each(
			function(idx){
				jQuery(linkArr[idx]).bind({
					click : function(e){
						doCommonStat("MAINMB70" + (_this.data.pageCntNum + 1) + "0" + (idx + 1));
					}
				});
				jQuery(linkArr[idx]).attr("href", this.URL1);
				jQuery(imageArr[idx]).attr("src", this.IMG1);
				jQuery(imageArr[idx]).attr("alt", this.TXT1);
			}
		);
	},
	rolling : function(){
		Main.deppartmentBrand.move(-1);
	},
	startRolling : function(){
		if(this.data.intervalObj === null){
			this.data.intervalObj = setInterval(this.rolling, 5000);
		}
	},
	stopRolling : function(){
		if(this.data.intervalObj !== null){
			clearInterval(this.data.intervalObj);
			this.data.intervalObj = null;
		}
	},

	logger : function(str){
		if(console) console.log(str);
	}
}

//개인화 영역
Main.personalArea = {
	data : {
		wrapper : "",
		productData : "",
		productRecommData : "",
		productDataCart : "",
		productRecommDataCart : "",
		productCnt : 0,
		productCntCart : 0,
		personalAreaMode : 0,
		currentPrdIdx : 0,
		currentPrdIdxCart : 0,
		relPrdViewCnt : 5,
		isNowLoading : false
	},

	init : function(){
		this.data.wrapper = jQuery("#psnlArea");
		if(funcCheckIsLogin()){
			this.getAjaxData("method=getPersonalAreaTotalData", "Main.personalArea.initData");
		}
	},

	getAjaxData : function(param, callback){

		this.data.isNowLoading = true;

		var url = "//www.11st.co.kr/browsing/MainAjax.tmall?" + param + "&callback="+callback;

		jQuery.ajax({
			url : url,
			dataType : 'jsonp',
			scriptCharset : 'UTF-8',
			jsonp : false
		});
	},

	initData : function(jsonData){

		var $wrapper = this.data.wrapper;
		if(jsonData.SHOW_AREA){
			if(jsonData.PRODUCT != null && jsonData.PRODUCT != undefined && jsonData.PRODUCT_TCT > 0 ){
				this.data.productData = jsonData.PRODUCT;
				this.data.productCnt = jsonData.PRODUCT_TCT;
				this.data.productRecommData = jsonData.RECOMM_PRODUCT;
			}

			if(jsonData.PRODUCT_CART != null && jsonData.PRODUCT_CART != undefined && jsonData.PRODUCT_TCT_CART > 0 ){
				this.data.productDataCart = jsonData.PRODUCT_CART;
				this.data.productCntCart = jsonData.PRODUCT_TCT_CART;
				this.data.productRecommDataCart = jsonData.RECOMM_PRODUCT_CART;
			}

			if(jsonData.SHOW_AREA){
				jQuery("#psnlNm", $wrapper).text(jsonData.memNm);
				jQuery($wrapper).show();
			}

			// 장바구니 & 최근상품
			if(this.data.productCnt > 0 && this.data.productCntCart > 0){
				this.data.personalAreaMode = 3;

				$wrapper.append(this.getPrdAreaHtml("cart_wrap", "장바구니", this.data.productCntCart, "cart"));

				$wrapper.append(this.getPrdAreaHtml("view_prd", "최근 본 상품", this.data.productCnt, "lately"));

				this.data.relPrdViewCnt = 2;

				if(this.data.productCnt > 1){
					this.setBtnHandler("lately");
				}

				if(this.data.productCntCart > 1){
					this.setBtnHandler("cart");
				}

				this.drawPrd(this.data.productDataCart, this.data.productRecommDataCart, "cart", 0);
				this.drawPrd(this.data.productData, this.data.productRecommData, "lately", 0);

				$wrapper.find("#psnlPrd_cart_0").show();
				$wrapper.find("#psnlPrd_lately_0").show();

				$wrapper.find("#psnlRecommPrd_cart_0").show();
				$wrapper.find("#psnlRecommPrd_lately_0").show();

				jQuery("#psnlPrd_cart_0").attr("dataLoad", "true");
				jQuery("#psnlPrd_lately_0").attr("dataLoad", "true");

				doCommonStat("MAINMB01LD01");
				hdStckPushSimple("WFMR1");

			// 장바구니
			}else if(this.data.productCntCart > 0){
				this.data.personalAreaMode = 2;

				$wrapper.addClass("type_only");

				$wrapper.append(this.getPrdAreaHtml("cart_wrap", "장바구니", this.data.productCntCart, "cart"));

				if(this.data.productCntCart > 1){
					this.setBtnHandler("cart");
				}

				this.drawPrd(this.data.productDataCart, this.data.productRecommDataCart, "cart", 0);

				$wrapper.find("#psnlPrd_cart_0").show();
				$wrapper.find("#psnlRecommPrd_cart_0").show();

				jQuery("#psnlPrd_cart_0").attr("dataLoad", "true");

				doCommonStat("MAINMB01LD02");
				hdStckPushSimple("WFBR1");

			// 최근 상품
			}else if(this.data.productCnt > 0){
				this.data.personalAreaMode = 1;

				$wrapper.addClass("type_only");

				$wrapper.append(this.getPrdAreaHtml("view_prd", "최근 본 상품", this.data.productCnt, "lately"));

				if(this.data.productCnt > 1){
					this.setBtnHandler("lately");
				}

				this.drawPrd(this.data.productData, this.data.productRecommData, "lately", 0);

				$wrapper.find("#psnlPrd_lately_0").show();
				$wrapper.find("#psnlRecommPrd_lately_0").show();

				jQuery("#psnlPrd_lately_0").attr("dataLoad", "true");

				doCommonStat("MAINMB01LD03");
				hdStckPushSimple("WFRR1");
			}

			hdStckFlush();
		}
	},

	drawPrd : function(prdData, recommPrdData, type, index){

		var clickCd1 = "MAINMB10";
		var clickCd2 = "";
		var trcCode = "";

		var recopickCnt = 0;

		if(this.data.personalAreaMode == 3){
			clickCd1 += "01";
			if(type == "cart"){
				trcCode = "WFMR1_";
			}else{
				trcCode = "WF2R1_";
			}
		}
		if(this.data.personalAreaMode == 2){
			clickCd1 += "11";
			trcCode = "WFBR1_";
		}
		if(this.data.personalAreaMode == 1){
			clickCd1 += "21";
			trcCode = "WFRR1_";
		}

		//최근 본 상품
		var prdAreaWrapper = this.data.wrapper.find("#psnlPrdArea_" + type);
		if(this.data.personalAreaMode == 3){
			prdAreaWrapper.find("#psnlPrd_" + type + "_" + index).append(this.getPrdHtml(prdData, clickCd1 + (type == "cart" ? "01" : "04"), ""));
		}else{
			prdAreaWrapper.find("#psnlPrd_" + type + "_" + index).append(this.getPrdHtml(prdData, clickCd1 + "01", ""));
		}

		for(var i = 0; i < this.data.relPrdViewCnt && i < recommPrdData.length; i++){
			if(recommPrdData[i].bReco){
				recopickCnt++;
			}
		}

		trcCode += "0" + recopickCnt;

		hdStckPushSimple(trcCode.replace(/_/gi, ""));

		//최근 본 상품(연관 상품)
		var recommPrdHtml = "";
		for(var i = 0; i < this.data.relPrdViewCnt && i < recommPrdData.length; i++){

			if(recommPrdData[i].bReco){
				clickCd2 = trcCode + "T0" + (i + 1) + "C" + recommPrdData[i].prdNo + "_" + prdData.prdNo;
				hdStckPushSimple(trcCode + "T0" + (i + 1) + "I" + recommPrdData[i].prdNo + "_" + prdData.prdNo);
			}else{
				clickCd2 = trcCode + "F0" + (i + 1) + "C" + recommPrdData[i].prdNo + "_" + prdData.prdNo;
				hdStckPushSimple(trcCode + "F0" + (i + 1) + "I" + recommPrdData[i].prdNo + "_" + prdData.prdNo);
			}

			if(this.data.personalAreaMode == 3){
				recommPrdHtml += "<li>" + this.getPrdHtml(recommPrdData[i], clickCd1 + (type == "cart" ? "0" + (i + 2) : "0" + (i + 5)), clickCd2) + "<\/li>";
			}else{
				recommPrdHtml += "<li>" + this.getPrdHtml(recommPrdData[i], clickCd1 + "0" + (i + 2), clickCd2) + "<\/li>";
			}
		}

		hdStckFlush();

		prdAreaWrapper.find("#psnlRecommPrd_" + type + "_" + index).append(recommPrdHtml);

		this.data.isNowLoading = false;
	},

	setBtnHandler : function(type){
		var _this = this;
		this.data.wrapper.find("#psnlBtn_" + type + "_prev").bind({
			click : function(e){
				if(!_this.data.isNowLoading){
					_this.prevPrdView(this.id);
				}
			}
		});

		this.data.wrapper.find("#psnlBtn_" + type + "_next").bind({
			click : function(e){
				if(!_this.data.isNowLoading){
					_this.nextPrdView(this.id);
				}
			}
		});
	},

	getProductData : function(jsonData){
		this.drawPrd(jsonData.PRODUCT, jsonData.RECOMM_PRODUCT, "lately", this.data.currentPrdIdx);
		jQuery("#psnlPrd_lately_" + this.data.currentPrdIdx).attr("dataLoad", "true");
	},

	getProductDataCart : function(jsonData){
		this.drawPrd(jsonData.PRODUCT_CART, jsonData.RECOMM_PRODUCT_CART, "cart", this.data.currentPrdIdxCart);
		jQuery("#psnlPrd_cart_" + this.data.currentPrdIdxCart).attr("dataLoad", "true");
	},

	getPrdAreaHtml : function(cssName, title, count, type){
		var prdAreaHtml = "";
		// cssName : view_prd -> 최근본 상품, cart_wrap -> 장바구니
		prdAreaHtml += "<div class=\"" + cssName + "\" id=\"psnlPrdArea_" + type + "\">";
		prdAreaHtml += "	<div class=\"main_prd\">";
		prdAreaHtml += "		<h2>" + title + "<span>(" + count + ")<\/span><\/h2>";
		prdAreaHtml += "		<div class=\"viewport\">";
		prdAreaHtml += "			<ul class=\"m_prdlist\">";
		for(var i = 0; i < count; i++){
			prdAreaHtml += "			<li id=\"psnlPrd_" + type + "_" + i + "\" style=\"display:none\" dataLoad=\"false\"><\/li>";
		}
		prdAreaHtml += "			<\/ul>";
		prdAreaHtml += "		<\/div>";
		if(count > 1){
			prdAreaHtml += "		<div class=\"btnctr_pn\">";
			prdAreaHtml += "			<button type=\"button\" class=\"in_prev\" id=\"psnlBtn_" + type + "_prev\">이전 상품<\/button>";
			prdAreaHtml += "			<button type=\"button\" class=\"in_next\" id=\"psnlBtn_" + type + "_next\">다음 상품<\/button>";
			prdAreaHtml += "		<\/div>";
		}
		prdAreaHtml += "	<\/div>";
		prdAreaHtml += "	<div class=\"about_prd\">";
		prdAreaHtml += "		<h3>연관추천상품<\/h3>";
		for(var i = 0; i < count; i++){
			prdAreaHtml += "			<ul class=\"m_prdlist\" id=\"psnlRecommPrd_" + type + "_" + i + "\" style=\"display:none\" dataLoad=\"false\"><\/ul>";
		}
		prdAreaHtml += "	<\/div>";
		prdAreaHtml += "<\/div>";

		return prdAreaHtml;
	},

	getPrdHtml : function(prdData, clickCd1, clickCd2){
		var prdHtml = "";
		prdHtml += "<a href=\"" + prdData.link + "\" onclick=\"doCommonStat('" + clickCd1 + "'); hdStckSimple('" + clickCd2 + "');\">";
		prdHtml += "	<img src=\"" + prdData.imgUrl + "\" alt=\"" + prdData.prdNm + "\" onerror=\"this.src='" + getCommonImgUrl( _IMG_URL_ + "/img/prd_size/noimg_110.gif") + "';\" width=\"110px\" height=\"110px\">";
		prdHtml += "	<p>" + prdData.prdNm + "<\/p>";
		prdHtml += "	<em>" + getCommaString(prdData.finalDscPrc) + "원<\/em>";
		prdHtml += "<\/a>";

		return prdHtml;
	},

	prevPrdView : function(btnId){
		var nowViewIdx = 0;
		var targetViewIdx = 0;

		//장바구니
		if("psnlBtn_cart_prev" == btnId){

			if(this.data.personalAreaMode == 3){
				doCommonStat("MAINMB100201");
			}else{
				doCommonStat("MAINMB101201");
			}

			nowViewIdx = this.data.currentPrdIdxCart;

			if(nowViewIdx == 0){
				targetViewIdx = this.data.productCntCart - 1;
			}else{
				targetViewIdx = this.data.currentPrdIdxCart - 1;
			}

			this.data.currentPrdIdxCart = targetViewIdx;

			if(jQuery("#psnlPrd_cart_" + targetViewIdx).attr("dataLoad") == "false"){
				this.getAjaxData("method=getPersonalProductDataCart&index=" + targetViewIdx + "&mainView=Y", "Main.personalArea.getProductDataCart");
			}

			jQuery("#psnlPrd_cart_" + nowViewIdx, "#psnlPrdArea_cart").hide();
			jQuery("#psnlPrd_cart_" + targetViewIdx, "#psnlPrdArea_cart").show();

			jQuery("#psnlRecommPrd_cart_" + nowViewIdx, "#psnlPrdArea_cart").hide();
			jQuery("#psnlRecommPrd_cart_" + targetViewIdx, "#psnlPrdArea_cart").show();
		}

		//최근 본 상품
		if("psnlBtn_lately_prev" == btnId){

			if(this.data.personalAreaMode == 3){
				doCommonStat("MAINMB100301");
			}else{
				doCommonStat("MAINMB102301");
			}

			nowViewIdx = this.data.currentPrdIdx;

			if(nowViewIdx == 0){
				targetViewIdx = this.data.productCnt - 1;
			}else{
				targetViewIdx = this.data.currentPrdIdx - 1;
			}

			this.data.currentPrdIdx = targetViewIdx;

			if(jQuery("#psnlPrd_lately_" + targetViewIdx).attr("dataLoad") == "false"){
				this.getAjaxData("method=getPersonalProductData&index=" + targetViewIdx + "&mainView=Y", "Main.personalArea.getProductData");
			}

			jQuery("#psnlPrd_lately_" + nowViewIdx, "#psnlPrdArea_lately").hide();
			jQuery("#psnlPrd_lately_" + targetViewIdx, "#psnlPrdArea_lately").show();

			jQuery("#psnlRecommPrd_lately_" + nowViewIdx, "#psnlPrdArea_lately").hide();
			jQuery("#psnlRecommPrd_lately_" + targetViewIdx, "#psnlPrdArea_lately").show();
		}
	},

	nextPrdView : function(btnId){
		var nowViewIdx = 0;
		var targetViewIdx = 0;

		//장바구니
		if("psnlBtn_cart_next" == btnId){

			if(this.data.personalAreaMode == 3){
				doCommonStat("MAINMB100202");
			}else{
				doCommonStat("MAINMB101202");
			}

			nowViewIdx = this.data.currentPrdIdxCart;

			if(nowViewIdx == this.data.productCntCart - 1){
				targetViewIdx = 0;
			}else{
				targetViewIdx = this.data.currentPrdIdxCart + 1;
			}

			this.data.currentPrdIdxCart = targetViewIdx;

			if(jQuery("#psnlPrd_cart_" + targetViewIdx).attr("dataLoad") == "false"){
				this.getAjaxData("method=getPersonalProductDataCart&index=" + targetViewIdx + "&mainView=Y", "Main.personalArea.getProductDataCart");
			}

			jQuery("#psnlPrd_cart_" + nowViewIdx, "#psnlPrdArea_cart").hide();
			jQuery("#psnlPrd_cart_" + targetViewIdx, "#psnlPrdArea_cart").show();

			jQuery("#psnlRecommPrd_cart_" + nowViewIdx, "#psnlPrdArea_cart").hide();
			jQuery("#psnlRecommPrd_cart_" + targetViewIdx, "#psnlPrdArea_cart").show();
		}

		//최근 본 상품
		if("psnlBtn_lately_next" == btnId){

			if(this.data.personalAreaMode == 3){
				doCommonStat("MAINMB100302");
			}else{
				doCommonStat("MAINMB102302");
			}

			nowViewIdx = this.data.currentPrdIdx;

			if(nowViewIdx == this.data.productCnt - 1){
				targetViewIdx = 0;
			}else{
				targetViewIdx = this.data.currentPrdIdx + 1;
			}

			this.data.currentPrdIdx = targetViewIdx;

			if(jQuery("#psnlPrd_lately_" + targetViewIdx).attr("dataLoad") == "false"){
				this.getAjaxData("method=getPersonalProductData&index=" + targetViewIdx + "&mainView=Y", "Main.personalArea.getProductData");
			}

			jQuery("#psnlPrd_lately_" + nowViewIdx, "#psnlPrdArea_lately").hide();
			jQuery("#psnlPrd_lately_" + targetViewIdx).show();

			jQuery("#psnlRecommPrd_lately_" + nowViewIdx).hide();
			jQuery("#psnlRecommPrd_lately_" + targetViewIdx).show();
		}
	}
}

Main.AdDsBnr = {
		description : "메인 DS서버 광고 영역",
		preFixUrl : "",
		useAdSwitch : "",
		mainLeftBigAdWhenSwitchOffed : "",
		useTrace : false,
		init : function(layerId , typeUrl ){
			try {
				this.chkUseAdSwitch();

				if (this.useAdSwitch) {

					var browser = navigator.userAgent.toLowerCase();
					var targetUrl = this.preFixUrl + '/NetInsight/text/11st/11st_main/main@'+ typeUrl + getNitmusParam(false);	// targeting으로

					if ( (browser.indexOf("iphone") >= 0) || (browser.indexOf("ipod") >= 0) || (browser.indexOf("ipad") >= 0) ) {  //Flash
						targetUrl = targetUrl+"_jpg";
					}
					jQuery.getScript(targetUrl);

				} else {
					// 대체로직 추가 : 차후에~~~
				}
			} catch(e) {
				this.trace(e.message);
			}
		},
		setPreFixUrl : function(preFixUrl){
			this.preFixUrl = preFixUrl;
		},
		setUseAdSwitch : function(useAdSwitch){
			this.useAdSwitch = useAdSwitch;
		},
		setBannerWhenSwitchOffed : function(mainLeftBigAdWhenSwitchOffed){
			this.mainLeftBigAdWhenSwitchOffed = mainLeftBigAdWhenSwitchOffed;
		},
		chkUseAdSwitch : function(){
			if(this.useAdSwitch == ""){
				this.useAdSwitch = _dsSeverMode;
			}
		},
		trace : function(msg){
			if(this.useTrace && console){
					console.log("[LOG] adDsBnr: ", msg);
			}
		}
	};
Main.dsRichAd = {
		description : "메인 DS서버 광고 영역",
		preFixUrl : "http://ds.11st.co.kr",
		useAdSwitch : '',
		mainLeftBigAdWhenSwitchOffed : '',
		useTrace : false,
		mouseOverTime : 0,
		isMouseOver : false,
		isDisplayAd : false,
		intervalObj : '',
		adData : '',
		me : '',
		adAreaId : '',
		intervalTime : 10,
		expandTime : 1500,

		init : function(){
			try {
				this.chkUseAdSwitch();
				if (this.useAdSwitch) {

					var targetUrl = this.preFixUrl + '/NetInsight/text/11st/11st_main/main@Main_Right' + getNitmusParam(false);	// targeting으로

					jQuery( document ).ready(function(){
						jQuery.ajax({
							url : targetUrl ,
							dataType : 'jsonp',
							jsonp : false,
							cache : false,
							jsonpCallback : 'Main.dsRichAd.dsCallback',
							success : function(data){
								if(data == null || typeof(data) == 'undefined' || data.type == ''){
									Main.dsRichAd.setReplace();
								}
							}
						});
					});

				} else {
					this.setReplace();
				}
			} catch(e) {
				this.trace(e.message);
			}
		},
		setPreFixUrl : function(preFixUrl){
			this.preFixUrl = preFixUrl;
		},
		setUseAdSwitch : function(useAdSwitch){
			this.useAdSwitch = useAdSwitch;
		},
		setBannerWhenSwitchOffed : function(mainLeftBigAdWhenSwitchOffed){
			this.mainLeftBigAdWhenSwitchOffed = mainLeftBigAdWhenSwitchOffed;
		},
		chkUseAdSwitch : function(){
			if(this.useAdSwitch == ""){
				this.useAdSwitch = _dsSeverMode;
			}
		},
		setReplace : function(){
			var $link = jQuery('#Main_ifrm_rightBnr').contents().find('#Main_link_rightSmallAd');
			$link.attr('href', 'http://www.44slimline.com/?mcode=08');
			$link.bind("click" , function(){
				doCommonStat('MAINMB60101');
			});

			jQuery('#Main_ifrm_rightBnr').contents().find('#Main_img_rightSmallAd').attr({
				'src' : 'http://i.011st.com/ds/2014/04/18/18/ab8997c77c7c408f408e8e00100dfe87.jpg',
				'alt' : '살빼는쉬운방법'
			});

		},
		setElement : function(){
			try{
				var samplingRate =   me.expandTime /  me.intervalTime;

				if(me.mouseOverTime < samplingRate){
					me.mouseOverTime ++;
				}

				var currentLoading = ( me.mouseOverTime / samplingRate  ) * 100 ;
				jQuery('#Main_ifrm_rightBnr').contents().find('#Main_span_rightAdLoading').css("width", currentLoading);

				if(me.mouseOverTime >= samplingRate && me.isMouseOver && !me.isDisplayAd){

					me.isDisplayAd = true;
					clearInterval(me.intervalObj);

					me.intervalObj = "";

					var $expAd = jQuery('#main_exp_ad');

					if ( $expAd.html() == '' ) {
						switch( me.adData.type ) {
							case 'A' :
								var html = '<a href="' + me.adData.extClick + '" target="_blank" onclick="doCommonStat(\'MAINMB60101\');"><img src="' + me.adData.extObj + '" alt="' + me.adData.extAlt + '"></a>';
								html += '	<button type="button" class="in_close" onclick="Main.dsRichAd.popupClose();">광고 닫기</button>';
								$expAd.html(html);
								break;
							case 'B' :
								$expAd.addClass('fullvideo');
							case 'C' :
								var html = '<div id= "main_exp_ad_player">';
								html += '<a href="' + me.adData.extClick + '" target="_blank" onclick="doCommonStat(\'MAINMB60101\');"><img src="' + me.adData.extReplaceImg + '" alt="' + me.adData.extAlt + '"></a>';
								html += '</div>';
								html += '<a id= "Main_link_rightAdFullPlayerScript" href="' + me.adData.scriptUrl + '" class="defbtn_sm dtype6" target="_blank"><span>대본다운로드</span></a>';
								html += '<button type="button" class="in_close" onclick="Main.dsRichAd.popupClose();">광고 닫기</button>';
								$expAd.html(html);

								var url = me.adData.extObj;
								var attributes = {};
								var flashvars = {LINKURL : me.adData.clickUrl};
								var params = { bgcolor:"", allowScriptAccess:"always", allowFullScreen:"false", quality:"high", wmode:"transparent", scale:"noscale", menu:"false", align:"left", salign:"t" };
								if ( me.adData.type == 'B'  ) {
									url = WWW_URL +  '/flash/richMedia/movie_11st.swf';
									flashvars.IMGURL = me.adData.extReplaceImg;
									flashvars.FLVURL = me.adData.extObj;
									//params.bgcolor = '#000000';
									//params.wmode = 'direct';
								}
								swfobject.embedSWF(url, "main_exp_ad_player", "826", "385", "9.0.115", "", flashvars, params, attributes);
								break;
						}
					}
					$expAd.css("display","block");
					doCommonStat('MAINMB60102');
					var img = new Image();
					img.src = me.adData.extImp;
				}
			}catch(e){
			}
		},
		dsCallback : function(data){
			try{
				me = this;
				if(data == null || typeof(data) == 'undefined' || data.type == ''){
					me.setReplace();
					return;
				}

				me.adData = data;
				// 모바일일때 일반형으로 처리
				if(isMobile == true){
					me.adData.type = 'D';
				}
				// iframe 영역 처리
				var $ifrmBnrWrapper =  jQuery('#Main_ifrm_rightBnr').contents();
				var $link = $ifrmBnrWrapper.find('#Main_link_rightSmallAd');
				$link.attr('href', me.adData.clickUrl);
				$link.bind({
					click : function(){
					doCommonStat('MAINMB60101');
					$link.keydown();
					},
					keydown : function(e){
						if(e.which == 13 && me.adData.type != 'D' && me.isDisplayAd == false){
							e.preventDefault();
							me.isMouseOver = true;
							me.mouseOverTime = me.expandTime / me.intervalTime;
							me.intervalObj = setInterval(me.setElement , me.intervalTime);
						}
					}
				});

				$ifrmBnrWrapper.find('#Main_img_rightSmallAd').attr({
					'src' : me.adData.baseImg,
					'alt' : me.adData.baseAlt
				});

				// 메인 확장형 광고
				if(me.adData.type != 'D'){
					$link.find('div').css('display' , 'block');
					if(me.adData.type == 'A'){
						$link.find('.desc').text('마우스를 올려주세요');
					}
					jQuery("#Main_layer_rightAd").bind({
						mouseover : function(){
							$link.find('.desc').css('display' , 'none');
							$link.find('.loading').css('display' , 'block');
							me.isMouseOver = true;
							me.mouseOverTime = 0;
							if(me.isDisplayAd == false ){
								me.intervalObj = setInterval(me.setElement , me.intervalTime);
							}
						},
						mouseleave : function(){
							me.isMouseOver = false;
							$link.find('.desc').css('display' , 'block');
							$link.find('.loading').css('display' , 'none');
							me.mouseOverTime = 0;
							$ifrmBnrWrapper.find('#Main_span_rightAdLoading').css("width", 0);
							clearInterval(me.intervalObj);
							me.intervalObj = "";
						}
					});
				}
			}catch(e){}

		},
		popupClose : function(type){
			var $expAd = jQuery('#main_exp_ad');
			$expAd.css("display","none");
			$expAd.html('');
			var $ifrmBnrWrapper =  jQuery('#Main_ifrm_rightBnr').contents();
			$ifrmBnrWrapper.find('.desc').css('display' , 'block');
			$ifrmBnrWrapper.find('.loading').css('display' , 'none');
			$ifrmBnrWrapper.find('#Main_span_rightAdLoading').css("width", 0);
			me.isDisplayAd = false;
		},
		openAd : function( url ){
			goStatUrl(url , 'MAINMB60101' , '_blank');
		},
		trace : function(msg){
			if(this.useTrace && console){
					console.log("[LOG] adDsBnr: ", msg);
			}
		}
};


//메인 빌보드영역
Main.billboard = {
		center : function(){
			var $wrapper = jQuery("#main_billboard");
			var titleData = {};
			var bnrData = {};
			var bnrSize = 0;
			var selectedMenuIdx = -1;
			var selectedTabIdx = 0;
			var $menuObjArr = {};
			var $titleListObjArr = {};
			var $titleObjArr = {};
			var $btnAdObjArr = {};
			var $btnPrdObjArr = {};
			var $playBtn = jQuery('.btn_autoplay', $wrapper);
			var $contObjArr = {};
			var options = {};
			var intervalObj = "";
			var maxPageNoArr = new Array(1,1,1,1,1,1,1);
			var currPageIdxArr = new Array(0,0,0,0,0,0,0);
			var currTabIdxArr = new Array(-1,-1,-1,-1,-1,-1,-1);
			var selectedPageNo = 0;
			var useTrace = false;
			var clickCd = 'MAINMB10';

			var setOptionObj = function(option){
				options = option;
			};
			var setTitleData = function(){
				titleData = eval("BillBoardBannerTitleList2016");
			};
			var setBnrData = function(){
				bnrData = getBnrData();
				bnrSize = bnrData.totalCount;
				maxPageNoArr[selectedMenuIdx] = Math.ceil(bnrSize/5);
			};
			var getBnrData = function(idx){

				if(idx == undefined){
					idx = selectedMenuIdx;
				}
				var result = "";
				if(idx > 3){


					try{
						if(typeof(eval("BillBoardAd2016_" + (idx + 1))) == "object" && eval("BillBoardAd2016_" + (idx + 1)).DATA != undefined){
							if(idx == 4){
								var bnrObj5 = eval("BillBoardBannerList2016_" + (idx + 1));
								var bnrObjAd5 = eval("BillBoardAd2016_" + (idx + 1));
								var tot = (bnrObj5.totalCount>2?2:bnrObj5.totalCount)+(bnrObjAd5.totalCount>8?8:bnrObjAd5.totalCount);
								result = {"DATA":[bnrObjAd5.DATA[3],bnrObjAd5.DATA[2],bnrObjAd5.DATA[1],bnrObjAd5.DATA[0],bnrObj5.DATA[Math.round(Math.random() * (bnrObj5.totalCount - 1))]
								                 ,bnrObjAd5.DATA[7],bnrObjAd5.DATA[6],bnrObjAd5.DATA[5],bnrObjAd5.DATA[4],bnrObj5.DATA[Math.round(Math.random() * (bnrObj5.totalCount - 1))]],"showCount":5,"totalCount":tot};
							}else{
								result = eval("BillBoardAd2016_" + (idx + 1));
							}
						}else{
							result = eval("BillBoardBannerList2016_" + (idx + 1));
						}
					}catch(e){
						result = eval("BillBoardBannerList2016_" + (idx + 1));
					}
				}else{
					result = eval("BillBoardBannerList2016_" + (idx + 1));
				}

				return result;
			};
			var setMenuObj = function(){
				$menuObjArr = jQuery("li[name=billboardMenu]", $wrapper);
			};
			var setTitleObj = function(){
				$titleListObjArr = jQuery("ul[name=titleList]", $wrapper);
				$titleObjArr = jQuery("li[name=title]", $wrapper);
			};
			var setButtonObj = function(){
				$btnAdObjArr = jQuery("button", $menuObjArr);
				$btnPrdObjArr = jQuery("button[name=BillPrdBtn]", $wrapper);
			};
			var setContObj = function(){
				$contObjArr = jQuery("div[name=billCont]", $wrapper);
			}
			var setMenuIdx = function(){
				// 노출 가중치
				var arrWeight = new Array();
				arrWeight.push(20);
				arrWeight.push(14.16);
				arrWeight.push(14.16);
				arrWeight.push(14.16);
				arrWeight.push(12.5);
				arrWeight.push(12.5);
				arrWeight.push(12.5);
				selectedMenuIdx = getRanNumWeight(arrWeight); //초기 랜덤 메뉴 index
			};
			var setTabIdx = function(){
				selectedTabIdx = currTabIdxArr[selectedMenuIdx];
				if(selectedTabIdx == -1 || selectedMenuIdx >= 5){
					selectedTabIdx = Math.round(Math.random() * (bnrSize - 1));
				}

				currTabIdxArr[selectedMenuIdx] = selectedTabIdx;
				currPageIdxArr[selectedMenuIdx] = Math.floor(selectedTabIdx/5);
			};
			var setTabIdxAD = function(){
				var currPageIdx = currPageIdxArr[selectedMenuIdx];
				var minCount = currPageIdx * 5;
				var maxCount = (currPageIdx + 1) * 5;
				if(maxCount > bnrSize -1 ){
					maxCount = bnrSize;
				}
				selectedTabIdx = minCount + Math.round(Math.random() * (maxCount - minCount - 1));
				currTabIdxArr[selectedMenuIdx] = selectedTabIdx;
				currPageIdxArr[selectedMenuIdx] = Math.floor(selectedTabIdx/5);
			};
			var drawTitle = function(){
				$menuObjArr.each(function(menuIdx){
					try{
						if(menuIdx === selectedMenuIdx){
							jQuery(this).addClass("on");
						}

						var currTitleData = titleData.DATA[menuIdx];
						jQuery("#Main_link_billboardTitle" + menuIdx ).text( currTitleData.TXT1);
						jQuery("#Main_link_billboardTitle" + menuIdx ).attr("data-ga-event-category", "PC_메인 body").attr("data-ga-event-action", "빌보드 탭 타이틀").attr("data-ga-event-label", (menuIdx+1)+"_탭_"+currTitleData.TXT1);
						
						var _that = this;

						jQuery("li[name=title]", jQuery(_that)).each(function(tabIdx){
							try{
								var currBnrObj = getBnrData(menuIdx);
								var currBnrData = currBnrObj.DATA[tabIdx];
								jQuery("a[name=link]", this).text(currBnrData.TXT1).attr("href", currBnrData.URL1).bind({
									click : function(evt){
                                        evt.preventDefault();
                                        evt.stopPropagation();
                                        rakeLog.sendRakeLog(this);
										var titleClickCd = clickCd + (menuIdx+1)  + '0' +(tabIdx +1);
										goStatUrl( currBnrData.URL1 , titleClickCd , '_blank');
									}
								});
								jQuery("a[name=link]", this).attr("data-ga-event-category", "PC_메인 body").attr("data-ga-event-action", "빌보드 탭 타이틀").attr("data-ga-event-label", (menuIdx+1)+"_탭_"+currBnrData.TXT1);
                                jQuery("a[name=link]", this).attr("data-log-body", "{'content_no':'" + currBnrData.BNRNO + "', 'trc_no':'" + currBnrData.TRCNO + "', 'content_name':'" + currBnrData.TXT1 + "', 'content_type':'BANNER', 'link_url':'" + currBnrData.URL1 + "'}");
                            }catch(e){
								jQuery(this).remove();
							}
						});

						if(menuIdx >= 4){

							var currBnrObj = getBnrData(menuIdx);
							var bnrTotalCnt = currBnrObj.totalCount
							if(bnrTotalCnt > 5){
								jQuery("button[idx=" + menuIdx + "]", $wrapper).show();
							}else{
								jQuery("button[idx=" + menuIdx + "]", $wrapper).hide();
							}
						}
					}catch(e){
						jQuery(this).remove();
					}
				});

				$contObjArr.each(function(menuIdx){
					var currTitleData = titleData.DATA[menuIdx];
					jQuery("h3", this).text(currTitleData.TXT1);
				});
			};
			var drawImage = function(){
				var $targetObjList = jQuery(jQuery("li[name=banner]", "#billboardBnr" + selectedMenuIdx));
				var $targetObj = jQuery($targetObjList.get(selectedTabIdx));
				var currBnrObj = getBnrData();
				var currBnrData = currBnrObj.DATA[selectedTabIdx];
				jQuery("li[name=banner]", $wrapper).hide();
				if(jQuery("a[name=link]", $targetObj).attr("href") === "#"){
					jQuery("a[name=link]", $targetObj).attr("href", currBnrData.URL1);
					jQuery("a[name=link]", $targetObj).attr("data-ga-event-category", "PC_메인 body").attr("data-ga-event-action", "빌보드 배너_클릭").attr("data-ga-event-label", "#"+(selectedMenuIdx+1)+"_"+currBnrData.TXT1);
                    jQuery("a[name=link]", $targetObj).attr("data-log-body", "{'content_no':'" + currBnrData.BNRNO + "', 'trc_no':'" + currBnrData.TRCNO + "', 'content_name':'" + currBnrData.TXT1 + "', 'content_type':'BANNER', 'link_url':'" + currBnrData.URL1 + "'}");
					jQuery("img[name=image]", $targetObj).attr("src", currBnrData.IMG1).attr("alt", getAltTxt(currBnrData.ETXT1,currBnrData.TXT1));
				}
				jQuery('#main_top').css('background-color', currBnrData.ETXT1);
				var $currMenuObj = jQuery("li[idx=" + selectedMenuIdx + "]", $wrapper);
				$currMenuObj.removeClass("on");
				jQuery($currMenuObj.get(selectedTabIdx)).addClass("on");
				$targetObjList.removeClass("on");
				$targetObj.addClass("on");
				$targetObj.show();
				
				window.rakeLog && window.rakeLog.scrollHandler();
			};
			var drawTab = function(){
				trace("drawTap");
				jQuery("li[page]", $wrapper).each(function(){
					var pageVal = jQuery(this).attr("page");
					if(pageVal == currPageIdxArr[selectedMenuIdx]){
						jQuery(this).addClass("on");
						jQuery(this).show();
					}else{
						jQuery(this).removeClass("on");
						jQuery(this).hide();
					}
				});
			};

			var visibleMenuObj = function(evt){
				var $currObj = jQuery(evt.currentTarget);
				trace($menuObjArr.index($currObj));
				selectedMenuIdx = $menuObjArr.index($currObj);
				$menuObjArr.removeClass("on");
				$menuObjArr.removeClass("focus");
				$currObj.addClass("on");
				$menuObjArr.find('ul').hide();
				jQuery("ul", $currObj).show();
				setBnrData();
				setTabIdx();
				drawTab();
				drawImage();
			}

			var InvisibleMenuObj = function(evt){
				$menuObjArr.each(function(idx){
					if(idx == selectedMenuIdx){
						jQuery(this).addClass("on");
						jQuery("ul", this).show();
					}else{
						jQuery(this).removeClass("on");
						jQuery("ul", this).hide();
					}
				});
			}

			var eventHandler = function(){
				$wrapper.bind({
					mouseenter : function(evt){
						stopRolling();
					},
					mouseleave : function(evt){
						if($playBtn.hasClass('stop')){
							startRolling();
						}
						trace("mouseleave");
						jQuery("ul", $menuObjArr).hide();
					},
					focusin : function(evt){
						stopRolling();
					},
					focusout : function(evt){
						startRolling();
					}
				});

				$menuObjArr.bind({
					mouseenter : function(evt){
						trace("$menuObjArr mouseenter");
						visibleMenuObj(evt);
					},
					focusin : function(evt){
						visibleMenuObj(evt);
					},
					focusout : function(evt){
						trace("$menuObjArr focusout");
						InvisibleMenuObj(evt);
					},
					mouseleave : function(evt){
						InvisibleMenuObj(evt);
					},
					click : function(evt){
						evt.preventDefault();
                        rakeLog.sendRakeLog(this);
						return false;
					}
				});

				$titleListObjArr.bind({
					mouseleave : function(evt){
						var $currObj = jQuery(evt.currentTarget);
						$currObj.hide();
						return false;
					}
				});
				
				$titleObjArr.bind({
					mouseenter : function(evt){
						trace("$titleObjArr mouseenter");
						var $currObj = jQuery(evt.currentTarget);
						var $currMenuObj = jQuery("li[idx=" + selectedMenuIdx + "]", $wrapper);
						selectedTabIdx = $currMenuObj.index($currObj);
						currTabIdxArr[selectedMenuIdx] = selectedTabIdx;
						$currMenuObj.removeClass("on");
						$currObj.addClass("on");
						drawImage();
					},
					focusin : function(evt){
						trace("$titleObjArr focusin");
						var $currObj = jQuery(evt.currentTarget);
						var $currMenuObj = jQuery("li[idx=" + selectedMenuIdx + "]", $wrapper);
						selectedTabIdx = $currMenuObj.index($currObj);
						currTabIdxArr[selectedMenuIdx] = selectedTabIdx;
						$currMenuObj.removeClass("on");
						$currObj.addClass("on");
						drawImage();
						stopRolling();
						return false;
					},
					focusout : function(evt){
						trace("$titleObjArr focusout");
						var $currObj = jQuery(evt.currentTarget);
						var $currMenuObj = jQuery("li[idx=" + selectedMenuIdx + "]", $wrapper);
						selectedTabIdx = $currMenuObj.index($currObj);
						if(selectedTabIdx == $currMenuObj.length -1){
							$currMenuObj.parent("ul").hide();
						}
						return false;
					}
				});

				$btnAdObjArr.bind({
					click : function(evt){
						trace("$btnAdObjArr click");
						if(evt.stopPropagation){
							evt.stopPropagation();
						}else{
							evt = evt || window.event;
							evt.cancelBubble = true;
						}

						var $currObj = jQuery(evt.currentTarget);
						var currIdx = jQuery($currObj).attr("idx");
						var btnType = jQuery($currObj).attr("name");
						var currPageIdx = currPageIdxArr[currIdx];
						var maxPageIdx = maxPageNoArr[currIdx] -1;

						if(btnType == "prev"){
							if(currPageIdx <= 0){
								currPageIdx = maxPageIdx;
							}else{
								currPageIdx --;
							}
						}else{
							if(currPageIdx >= maxPageIdx){
								currPageIdx = 0;
							}else{
								currPageIdx++;
							}
						}
						trace(currPageIdx);
						currPageIdxArr[currIdx] = currPageIdx;
						setBnrData();
						setTabIdxAD();
						drawTab();
						drawImage();
                        rakeLog.sendRakeLog(this);
					}
				});

				$btnPrdObjArr.bind({
					click : function(evt){
						var $currObj = $menuObjArr[selectedMenuIdx];
						var maxMenuSize = $menuObjArr.length - 1;
						selectedMenuIdx = $menuObjArr.index($currObj);
						var btnType = jQuery(evt.currentTarget).attr("class");
						if(btnType == "btn_paging btn_next"){
							if( bnrSize == selectedTabIdx + 1 ) {
								if(selectedMenuIdx == maxMenuSize ){
									selectedMenuIdx = 0;
								}else{
									selectedMenuIdx++;
								}
								setBnrData();
								drawTab();
								selectedTabIdx = 0;
								$currObj = $menuObjArr[selectedMenuIdx];

							}else{
								selectedTabIdx ++;
							}
							doCommonStat('MAINMB11002');
						}else{
							if(  selectedTabIdx  == 0) {
								if( selectedMenuIdx == 0){
									selectedMenuIdx = maxMenuSize ;
								}else{
									selectedMenuIdx --;
								}
								setBnrData();
								drawTab();
								selectedTabIdx = bnrSize - 1;
								$currObj = $menuObjArr[selectedMenuIdx];

							}else{
								selectedTabIdx --;
							}
							doCommonStat('MAINMB11001');
						}

						trace("selectedTabIdx :" +  selectedTabIdx );
						trace("selectedMenuIdx :" +  selectedMenuIdx );
						currTabIdxArr[selectedMenuIdx] = selectedTabIdx;

						$menuObjArr.removeClass("on");
						jQuery($currObj).addClass("on");

						drawImage();
					}
				});
				
				$playBtn.bind({
					click : function(evt){
						if($playBtn.hasClass('play')){
							startRolling();
							$playBtn.removeClass('play').addClass('stop');
							jQuery('button', $playBtn).attr("data-ga-event-category", "PC_메인 body").attr("data-ga-event-action", "일시정지/자동재생 버튼").attr("data-ga-event-label","일시정지 버튼 클릭");
						}else if($playBtn.hasClass('stop')){
							stopRolling();
							$playBtn.removeClass('stop').addClass('play');
							jQuery('button', $playBtn).attr("data-ga-event-category", "PC_메인 body").attr("data-ga-event-action", "일시정지/자동재생 버튼").attr("data-ga-event-label","자동재생 버튼 클릭");
						}
					}
				});
			};

			var rolling = function(){
				if(bnrSize > 1){
					if(selectedTabIdx >= bnrSize-1){
						selectedTabIdx = -1;
					}
					selectedTabIdx ++;
					currTabIdxArr[selectedMenuIdx] = selectedTabIdx;
					drawImage();
					trace("Rolling");
				}
			};
			var startRolling = function(){
				if(options.auto){
					if(!intervalObj){
						if(!options.interval){
							options.interval = 5000;
						}
						trace("startRolling");
						intervalObj = setInterval(rolling, options.interval);
					}
				}
			};
			var stopRolling = function(){
				clearInterval(intervalObj);
				intervalObj = "";
				trace("stopRolling");
			};
			
			var setGATaging = function(){
				jQuery( document ).ready(function(){
					try {
						jQuery("a", $wrapper).each(function(idx, obj) {
							jQuery(obj).bind('click',catchAnchorGAToEvent);
						});
					} catch(e) {
					}
					try {
						jQuery('button', $wrapper).each(function(idx, obj) {
							jQuery(obj).bind('click',catchAnchorGAToEvent);
						});
					} catch(e) {
					}
				});
			};

			var initialize = function(){
				setOptionObj({auto:true});
				setMenuObj();
				setTitleObj();
				setButtonObj();
				setContObj();
				setTitleData();
				setMenuIdx();
				setBnrData();
				setTabIdx();
				drawTitle();
				drawImage();
				startRolling();
				eventHandler();
				setGATaging();
			};

			var trace = function(msg){
				if(useTrace){
					if(console){
						console.log("[LOG] MainBillBoard: ", msg);
					}
				}
			};

			return {
				init : function(){
					initialize();
				}
			}
		},
		right : function(){
			var $wrapper = jQuery('#main_hot_top');
			var selectedIdx = 0;
			var intervalObj = "";
			var $hotKeywdUlArr = jQuery('#search_hot_list ul', $wrapper);
			var $personTabLiArr = jQuery('#main_person_hot .tab li', $wrapper);
			var $personContUlArr = jQuery('#main_person_hot .cont ul', $wrapper);
			
			var changePage = function(){
				jQuery($hotKeywdUlArr).hide();
				jQuery($hotKeywdUlArr[selectedIdx]).show();
				jQuery($hotKeywdUlArr[selectedIdx+1]).show();
			}
			var rolling = function(){
				if(selectedIdx >= 4){
					selectedIdx = 0;
				}else{
					selectedIdx = selectedIdx+2;
				}
				changePage();
			};
			var startRolling = function(){
				intervalObj = setInterval(rolling, 5000);
			};
			
			var stopRolling = function(){
				clearInterval(intervalObj);
				intervalObj = "";
			};
			
			var eventHandler = function(){
				var $hotKeywordBtn = jQuery('button[name=hotKeywordBtn]',$wrapper);
				$hotKeywordBtn.bind({
					click : function(evt){
						evt.preventDefault();
						var $thisObj = jQuery(evt.target);
						if(jQuery($thisObj).hasClass('btn btn_prev')){
							if(selectedIdx <= 0){
								selectedIdx = 4;
							}else{
								selectedIdx = selectedIdx-2;
							}
						}else{
							if(selectedIdx >= 4){
								selectedIdx = 0;
							}else{
								selectedIdx = selectedIdx+2;
							}
						}
						changePage();
					},
					mouseenter : function(evt){
						stopRolling();
					},
					mouseleave : function(evt){
						startRolling();
					}
				});
				
				$hotKeywdUlArr.bind({
					mouseenter : function(evt){
						stopRolling();
					},
					mouseleave : function(evt){
						startRolling();
					}
				});
				
				$personTabLiArr.bind({
					mouseenter : function(evt){
						var $currObj = jQuery(evt.currentTarget);
						var selectedTabIdx = $personTabLiArr.index($currObj);
						jQuery($personContUlArr).hide();
						jQuery($personContUlArr[selectedTabIdx]).show();
						$personTabLiArr.removeClass("on");
						$currObj.addClass("on");
					}
				});
			};
			
			var setGATaging = function(){
				jQuery( document ).ready(function(){
					try {
						jQuery("a", $wrapper).each(function(idx, obj) {
							jQuery(obj).bind('click',catchAnchorGAToEvent);
						});
					} catch(e) {
					}
				});
			};
			
			var initialize = function(){
				startRolling();
				eventHandler();
				setGATaging();
				
				//성별
				var genderCd = TMCookieUtil.getSubCookie(0, 'GND');
				if(genderCd == '10'){
					jQuery($personContUlArr).hide();
					jQuery($personContUlArr[0]).show();
					$personTabLiArr.removeClass("on");
					jQuery($personTabLiArr.get(0)).addClass("on");
				}
			};
			
			return {
				init : function(){
					initialize();
				}
			}
		},
		right_v2 : function(){
			var recommendIntervalObj = "";
			var recommendPrdLength = 0;
			var recommendPageIdx = 0;
			
			var recommendPrd = function(){
				var _this = this;
				jQuery.ajax({
				    url : '//www.11st.co.kr/browsing/MainAjax.tmall?method=getRecopickUserInfoJson',
				    type : 'get',
				    dataType : 'json',
				    success : function (data) {
				    	
				    	var initSubTabIdxArr = [];
				    	initSubTabIdxArr[0] = Math.floor(Math.random() * data.prdList.length);;

				    	data["initSubTabIdx"] = initSubTabIdxArr;
				    	
				    	var buffer = [];
				    	buffer.push(getHandleBarsMarkup("main_billboard_right_template_v2", data));
				    	
				    	var recommPrdCarrDiv = jQuery("#main_recommend"); 
				    	recommPrdCarrDiv.append(buffer.join(''));
				    	
				    	try{
							jQuery("a", recommPrdCarrDiv).each(function(idx, obj) {
								jQuery(obj).bind('click', catchAnchorGAToEvent);
							});
						}catch(e){}
						
						if(data.prdType == '0'){
							var prdEl = jQuery('ul.product_list_col_2', recommPrdCarrDiv).children();
							_this.recommendPrdLength = prdEl.length;
							_this.recommendPageIdx = 0;
							
							if(_this.recommendPrdLength > 4){
								recommPrdCarrDiv.bind({
									mouseenter : function(){
										recommendStopRolling();
									},
									mouseleave : function(){
										recommendStartRolling();
									}
								});
								
								recommendStartRolling();
							}
						}
				    },
				    complete : function(data) {
				    	if (!(data.responseJSON) || (typeof data.responseJSON !== "object")) {
				    		recommendPrd();
			            }
		            }
				});
			};
			
			var showRecommendPrd = function(pageIdx){
				var _this = this;
				var recommPrdCarrDiv = jQuery("#main_recommend");
				var prdEl = jQuery('ul.product_list_col_2', recommPrdCarrDiv).children();
				var pageButton = jQuery('div.bnr_exhibition_paging', recommPrdCarrDiv).children();
				
				_this.recommendPageIdx = pageIdx;
				
				var startIdx = _this.recommendPageIdx * 4;
				var endIdx = startIdx + 4;
				if(endIdx + 1 >= _this.recommendPrdLength){
					endIdx = _this.recommendPrdLength;
				}
				
				prdEl.hide();
				pageButton.removeClass("on");
				jQuery(pageButton[pageIdx]).addClass("on");
				for(var i = startIdx; i < endIdx; i++){
					jQuery(prdEl[i]).show();
				}
			};
			
			var showNextRecommendPrd = function(){
				var _this = this;
				if(_this.recommendPageIdx + 1 == _this.recommendPrdLength / 4){
					_this.recommendPageIdx = 0;
				}else{
					_this.recommendPageIdx++;
				}
				
				showRecommendPrd(_this.recommendPageIdx);
			};
			
			var recommendStopRolling = function(){
				var _this = this;
				if(_this.recommendIntervalObj){
					clearInterval(_this.recommendIntervalObj);
					_this.recommendIntervalObj = "";
				}
			};
			
			var recommendStartRolling = function(){
				var _this = this;
				if(_this.recommendPrdLength > 4 && !_this.recommendIntervalObj){
					_this.recommendIntervalObj = setInterval(showNextRecommendPrd, 5000);
				}
			};
			
			var getHandleBarsMarkup = function(id, jsonData){
				try{
					if(jQuery("#" + id)){
					    var source = jQuery("#" + id).html();
					    var template = Handlebars.compile(source);
					    return template(jsonData);
					}
					return '';
				}catch(e){
					console.log(e);
				}
			};
			
			var displayBlock = function(blockIndex){
				var carrDiv = jQuery("#main_recommend");
				jQuery("div[divType=block]", carrDiv).hide();
				
				var blockDiv = jQuery("div[name=block_" + blockIndex + "]", carrDiv); 
				blockDiv.show();
				
				var blockTab = jQuery("li[name=blockTab]", carrDiv); 
				blockTab.removeClass("on");
				jQuery(blockTab[blockIndex]).addClass("on");
			};
			
			return {
				init : function(){
					Handlebars.registerHelper('jsonToString', function (object) {
						return JSON.stringify(object);
					});
					
					recommendPrd();
				},
				displayBlock : function(blockIndex){
					displayBlock(blockIndex);
				},
				showRecommendPrd : function(pageIdx){
					showRecommendPrd(pageIdx);
				}
			};
		} 
};

/*
 * Main Key Promotion
 * */
Main.keyPromotion = {
	init : function(){
		var $promotionArea = jQuery("#mainPromotion_area");
		if( typeof(keyPromotionBnnr) != 'undefined' && keyPromotionBnnr.totalCount > 0 ) {
			try{
				var DATA_LIST = keyPromotionBnnr.DATA;
				var crg_num = keyPromotionBnnr.totalCount;
				var ran = Math.floor(Math.random()*crg_num);
				var bannerData = DATA_LIST[ran];
				var $div = jQuery('<div class="inner"></div>');
                var $a = jQuery('<a />')
                    .attr('href', 'javascript:goStatUrl(\''+bannerData.URL1+'\', \'MAINMKP0101\')')
                    .attr("data-ga-event-category", "PC_메인 body")
                    .attr("data-ga-event-action", "키 프로모션 배너")
                    .attr("data-ga-event-label", bannerData.URL1)
                    .attr('onclick', "rakeLog.sendRakeLog(this);")
                    .attr("data-log-body", "{'content_no':'" + bannerData.BNRNO + "', 'trc_no':'" + bannerData.TRCNO + "', 'content_name':'" + bannerData.TXT1 + "', 'content_type':'BANNER', 'link_url':'" + bannerData.URL1 + "'}");
				var $img = jQuery('<img />').attr('src', bannerData.IMG1).attr('alt', bannerData.TXT1);
				$div.append($a.append($img));
				$promotionArea.css('background-color', bannerData.ETXT1);
				$promotionArea.append($div);
				$promotionArea.show();
			}
			catch(e) {}
			jQuery( document ).ready(function(){
				try {
					jQuery("a", $promotionArea).each(function(idx, obj) {
						jQuery(obj).bind('click', catchAnchorGAToEvent);
					});
				} catch(e) {
				}
			});
		} else {
			$promotionArea.hide();
		}
	}
};

Main.majorService =  (function(){
	var $wrapper = {};
	var currData = {};
	var dataIdx = 0;
	var majorServiceDispCnt = 6;
	var $majorServiceUl = {};
	var $currPrdUl = {};
	var selectedUlIdx = 0;
	var maxPageIdx = 2;
	var $paging = {};
	
	var setWrapper = function(){
		$wrapper = jQuery('#main_major_service');
		$majorServiceUl = jQuery('ul[name=majorServiceUl]', $wrapper);
		$paging = jQuery('#paging', $wrapper);
		if( (majorServiceJson.length / majorServiceDispCnt ) > 3 ){
			maxPageIdx = 2;
		}else{
			maxPageIdx = parseInt( (majorServiceJson.length / majorServiceDispCnt ) - 1);
			if(maxPageIdx < 0 && majorServiceJson.length > 0){
				maxPageIdx = 0;
			}
		}
	};
	var drawPageing = function(){
		$paging.append('<span class="num"><strong><span class="skip">현재 페이지</span><span id="currentPageNo">1</span></strong> / <span class="skip">총 페이지</span>' + (maxPageIdx + 1) + '</span>');
		$paging.append('<button name="pagingBtn" type="button" class="btn btn_prev" data-log-body="{\'direction\':\'left\'}">이전 리스트 보기</button>');
		$paging.append('<button name="pagingBtn" type="button" class="btn btn_next" data-log-body="{\'direction\':\'right\'}">다음 리스트 보기</button>');
		
		var $pagingBtns = jQuery('button[name=pagingBtn]', $paging);
		$pagingBtns.bind({
			click : function(evt){
				evt.preventDefault();
				var $thisObj = jQuery(evt.target);
				if(jQuery($thisObj).hasClass('btn btn_prev')){
					if(selectedUlIdx <= 0){
						selectedUlIdx = maxPageIdx;
					}else{
						selectedUlIdx = selectedUlIdx -1;
					}
				}else{
					if(selectedUlIdx >= maxPageIdx){
						selectedUlIdx = 0;
					}else{
						selectedUlIdx = selectedUlIdx + 1;
					}
				}
				changePage();
			}
		});
	};
	var setPageing = function(){
		jQuery('#currentPageNo', $paging).html(selectedUlIdx + 1);
	};
	var setCurrData = function(){
		try{
			dataIdx = majorServiceDispCnt * selectedUlIdx;
			for(var i = 0; i < majorServiceDispCnt; i++){
				if( dataIdx < majorServiceJson.length){
					currData[i] = majorServiceJson[dataIdx];
					dataIdx++;
				}
			}
		}catch(e){
		}
	};
	var setCurrUlObj = function(){
		$currPrdUl = jQuery($majorServiceUl.get(selectedUlIdx));
	};
	var drawProduct = function(){
		var $currPrdObjArr =jQuery('li[name=majorService]', $currPrdUl );
		var startIdx = 0;
		for(var loopIdx = 0; loopIdx < majorServiceDispCnt; loopIdx++){
			try{
				var currPrdIdx = startIdx + loopIdx;
				var currPrdData = currData[currPrdIdx];
				var $targetObj = jQuery($currPrdObjArr.get(currPrdIdx));

				if(jQuery('a[name=link]', $targetObj).attr('href') == '#'){
					jQuery('a[name=link]', $targetObj).attr('href', currPrdData.URL1);
					jQuery('a[name=link]', $targetObj).attr({'data-ga-event-category': 'PC메인_body 모듈외', 'data-ga-event-action':'11번가 주요서비스', 'data-ga-event-label':currPrdData.TXT1});
					jQuery('img[name=image]', $targetObj).attr({'src' : currPrdData.IMG1, 'onerror' : 'javascript:this.src=\'' +_IMG_URL_+ '/img/prd_size/noimg_100.gif\';', 'alt': currPrdData.TXT1});
				}
			}catch(e){
				jQuery(this).remove();
			}
		}
	};
	var changePage = function(){
		setCurrUlObj();
		setCurrData();
		drawProduct();
		$majorServiceUl.hide();
		jQuery($majorServiceUl.get(selectedUlIdx)).show();
		setPageing();
	};
	var setGATaging = function(){
		jQuery( document ).ready(function(){
			try {
				jQuery('a', $wrapper).each(function(idx, obj) {
					jQuery(obj).bind('click', catchAnchorGAToEvent);
				});
			} catch(e) {
			}
		});
	};
	var initialize = function(){
		setWrapper();
		drawPageing();
		selectedUlIdx = Math.round(Math.random() * maxPageIdx);
		changePage();
		setGATaging();
	};
	
	return {
		init : function(){
			initialize();
		}
	}
})();

Main.best =  (function(){
	var $wrapper = {};
	var currData = {};
	var currShowCnt = 6;
	var selectedMetaIdx = 0;
	var selectedRankIdx = 0;
	var maxRankIdx = 2;
	var $metaObjArr = {};
	var $currMetaObj = {};
	var $prdUlArr = {};
	var $currPrdUl = {};
	var useTrace = false;
	var regnCdArr = new Array("MAINMB502","MAINMB503","MAINMB504","MAINMB505","MAINMB506","MAINMB507","MAINMB508","MAINMB509","MAINMB510","MAINMB511", "MAINMB501");
    var prdTypeCounsel = "counsel";

	var setWrapper = function(){
		$wrapper = jQuery("#main_best");
	};

	var setMetaIdx = function(){
		selectedMetaIdx = Math.round(Math.random() * (MainRanking.length  - 2 ));
	};
	var setMetaObj = function(){
		$metaObjArr = jQuery("li[name=bestMeta]", $wrapper);
		$prdUlArr = jQuery("ul[name=main_best_list]", $wrapper);
	};
	var setCurrMetaObj = function(){
		$currMetaObj = jQuery($metaObjArr.get(selectedMetaIdx));
		$currPrdUl = jQuery($prdUlArr.get(selectedMetaIdx));
	};
	var setCurrData = function(){
		try{
			currData = MainRanking[selectedMetaIdx + 1];
		}catch(e){
		}
	};

	var changeRankingObj = function(){
		setMetaObj();
		$metaObjArr.removeClass("on");
		setCurrMetaObj();
		setCurrData();
		selectedRankIdx = 0;
		drawMeta();
		drawProduct();
	};
	var eventHandler = function(){
		$metaObjArr.bind("mouseover focusin", function(evt){
				trace("mouseover");
				evt.preventDefault();
				var $thisObj = jQuery(evt.target);
				if($thisObj.is("a[name=bestTitle]") || $thisObj.is("#Main_link_BestTotal")){
					selectedMetaIdx = $metaObjArr.index(jQuery(evt.currentTarget));
					changeRankingObj();
				}
		});
		var $rankBtn = jQuery("button[name=bestPrdBtn]",$wrapper);
		$rankBtn.bind({
			click : function(evt){
				evt.preventDefault();
				var $thisObj = jQuery(evt.target);
				if(jQuery($thisObj).hasClass('btn_prev')){
					selectedRankIdx -- ;
					if(selectedRankIdx < 0 ){
						selectedRankIdx = maxRankIdx;
					}
					doCommonStat("MAINMB51201");
				}else{
					selectedRankIdx++;
					if(selectedRankIdx > maxRankIdx ){
						selectedRankIdx = 0;
					}
					doCommonStat("MAINMB51202");
				}
				trace("selectedRankIdx" + selectedRankIdx);
				drawProduct();
			}
		});
		var $rankTotal = jQuery("#Main_link_BestTotal");
		$rankTotal.bind("mouseover focusin" , function(evt){
			trace("total mouseover");
			evt.preventDefault();
			var $thisObj = jQuery(evt.target);

			currData = MainRanking[0];

			selectedMetaIdx = 10;
			$currPrdUl = jQuery($prdUlArr.get(selectedMetaIdx));
			drawMeta('total');
			drawProduct();
		});
	};
	var setGATaging = function(){
		jQuery( document ).ready(function(){
			try {
				jQuery("#main_hot_best a").each(function(idx, obj) {
					jQuery(obj).bind('click', catchAnchorGAToEvent);
				});
			} catch(e) {
			}
		});
	};
	
	var initialize = function(){
		setWrapper();
		setMetaIdx();
		setMetaObj();
		setCurrMetaObj();
		setCurrData();
		drawMeta();
		drawProduct();
		eventHandler();
		setGATaging();
	};
	var drawMeta = function(tab){
		$metaObjArr.removeClass("on");
		jQuery("ul[name=main_best_list]", $wrapper).hide();

		if(tab == 'total'){
			$currMetaObj =jQuery("#Main_link_BestTotal") ;
			jQuery(jQuery("ul[name=main_best_list]", $wrapper).get(selectedMetaIdx)).show();
		}else{
			jQuery(jQuery("ul[name=main_best_list]", $wrapper).get(selectedMetaIdx)).show();
		}
		$currMetaObj.addClass("on");
		$currMetaObj.show();
	};
	var drawProduct = function(){

		var $currPrdObjArr =jQuery("li[name=product]", $currPrdUl );
		$currPrdObjArr.hide();
		var startIdx = selectedRankIdx * currShowCnt;
		regnCd = regnCdArr[selectedMetaIdx];
		var conerIdx = selectedMetaIdx + 1;
		var prdUrl = 'http://www.11st.co.kr/browsing/BestSeller.tmall?method=getBestSellerCornerMain&cornerNo='+conerIdx +'&viewType=I'
        for(var loopIdx=0; loopIdx<currShowCnt; loopIdx++){
            try{
                var currPrdIdx = startIdx + loopIdx;
                var currPrdData = currData.DATA[currPrdIdx];
                var $targetObj = jQuery($currPrdObjArr.get(currPrdIdx));

                if(jQuery("a[name=link]", $targetObj).attr("href") == "#"){
                    var url = "javascript:goStatPrdUrl('" + prdUrl + "','" + currPrdData.NUM1 + "','" +regnCd + "0" +((currPrdIdx % 4 ) +2 ) + "');";
                    jQuery("a[name=link]", $targetObj).attr("href", url);
                    jQuery("a[name=link]", $targetObj).attr("data-ga-event-category", "PC메인_body 모듈외").attr("data-ga-event-action", "11번가베스트_상품").attr("data-ga-event-label", currPrdData.NUM1 + '_' + currPrdData.TXT1);
                    jQuery("a[name=link]", $targetObj).attr("data-log-body", "{'content_no':'" + currPrdData.NUM1 + "', 'content_name':'" + currPrdData.TXT1 + "', 'content_type':'PRODUCT'}");
                    jQuery("a[name=link]", $targetObj).attr("data-log-index", currPrdIdx+1);
                    jQuery("img[name=image]", $targetObj).attr({"src" : currPrdData.IMG1, "alt" : currPrdData.TXT1});
                    jQuery("span[name=prdName]", $targetObj).text(currPrdData.TXT1);
                    //jQuery("span[name=price]", $targetObj).html(currPrdData.PRC1);
                    //jQuery("span[name=optTxt]", $targetObj).html(currPrdData.OPT_TXT);

                    if(currPrdData.expansInfo) {
                        jQuery('span[class=info]', $targetObj).html('<span class="consult_txt">' + currPrdData.expansInfo.infoText1 + '</span>');
                        jQuery('span[class=consult_txt]', $targetObj).css('color', currPrdData.expansInfo.infoColor1);
                    } else {
                        jQuery('span[class=info]', $targetObj).html('<span class="price"><span class="skip">판매가</span><span name="price" class="n">'+ currPrdData.PRC1 +'</span>원 <span name="optTxt">'+ currPrdData.OPT_TXT +'</span>');
                    }

                    window.rakeLog && window.rakeLog.scrollHandler();
                }
                $targetObj.show();
            }catch(e){
                jQuery(this).remove();
            }
        }

	};

	var trace = function(msg){
		if(window.console && useTrace) console.log("ranking: ", msg);
	};

	return {
		init : function(){
			initialize();
		}
	}
})();

Main.shockingDeal =  (function(){
	var $wrapper = {};
	var $shocking_box = {};
	var $shocking_list = {};
	var dispData = {};
	var currData = {};
	var emergencyPrdData = {};
	var emergencyPrdDataSize = 0;
	var emergencyPrdDataIdx = 0;
	var prdData = {};
	var prdDataSize = 0;
	var prdDataIdx = 0;
	var prdDispCnt = 8;
	var $prdUlArr = {};
	var $currPrdUl = {};
	var selectedUlIdx = 0;
	var maxPageIdx = 2;
	var $paging = {};
    var prdTypeCounsel = "counsel";
    var cupnIssObjCdExhibition = '04';
    var cupnIssObjCdStoreCart = '10';
    var cupnIssObjCdStore = '03';

	var setWrapper = function(){
		$wrapper = jQuery('#main_shocking');
		$shocking_box = jQuery('#shocking_box', $wrapper);
		$shocking_list = jQuery('#shocking_list', $wrapper);
		$prdUlArr = jQuery('ul[name=prdUl]', $shocking_list);
		$paging = jQuery('#paging', $wrapper);
	};
	var setData = function(){
		try {
			jQuery.ajax({
			    url : '//www.11st.co.kr/browsing/MainAjax.tmall?method=getShockingDealPrdJson',
			    type : 'post',
			    dataType : 'json',
			    success : function (data) {
			    	if(data != null || data != '' || typeof(data) != 'undefined'){
			    		emergencyPrdData = data.shockingDealEmergencyPrdListJson.items;
			    		emergencyPrdDataSize = data.shockingDealEmergencyPrdListJson.count;
						prdData = data.shockingDealPrdListJson.items;
						prdDataSize = data.shockingDealPrdListJson.count;
						if( ((emergencyPrdDataSize + prdDataSize) / 8 ) > 4 ){
							maxPageIdx = 3;
						}else{
							maxPageIdx = parseInt( ((emergencyPrdDataSize + prdDataSize) / 8 ) - 1);
						}
						for(var i = 0; i < 32; i++){
							if( emergencyPrdDataIdx < emergencyPrdDataSize){
								dispData[i] = emergencyPrdData[emergencyPrdDataIdx];
								dispData[i].isEmergencyPrd = 'Y';
								emergencyPrdDataIdx++;
							}else if( prdDataIdx < prdDataSize){
								dispData[i] = prdData[prdDataIdx];
								prdDataIdx++;
							}
						}
						drawPageing();
						setPageing();
						setCurrData();
						setCurrUlObj();
						drawProduct();
					}
			    }
			});
		} catch(e) {
		}
	};
	var drawPageing = function(){
		for(var i = 0; i <= maxPageIdx; i++ ){
			$paging.append('<button name="pagingBtn" type="button" class="ico">쇼킹딜 상품목록 ' + (i+1) + '</button>');
		}
		var $pagingBtns = jQuery('button[name=pagingBtn]', $paging);
		$pagingBtns.bind({
			click : function(evt){
				var $currObj = jQuery(evt.currentTarget);
				selectedUlIdx = $pagingBtns.index($currObj);
				changePage();
			}
		});
	};var setPageing = function(){
		jQuery('button', $paging).removeClass('on');
		jQuery(jQuery('button', $paging).get(selectedUlIdx)).addClass('on');
	};
	var setCurrData = function(){
		try{
			for(var i = 0; i < prdDispCnt; i++){
				currData[i] = dispData[( (prdDispCnt * selectedUlIdx) + i )];
			}
		}catch(e){
		}
	};
	var setCurrUlObj = function(){
		$currPrdUl = jQuery($prdUlArr.get(selectedUlIdx));
	};
	var drawProduct = function(){
		var $currPrdObjArr =jQuery('li[name=product]', $currPrdUl );
		var startIdx = 0;
		for(var loopIdx=0; loopIdx<prdDispCnt; loopIdx++){
			try{
				var currPrdIdx = startIdx + loopIdx;
				var currPrdData = currData[currPrdIdx];
				var $targetObj = jQuery($currPrdObjArr.get(currPrdIdx));

				if(jQuery('a[name=link]', $targetObj).attr('href') == '#'){
					jQuery('a[name=link]', $targetObj).attr('href', currPrdData.url1);
					jQuery('a[name=link]', $targetObj).attr('data-ga-event-category', 'PC메인_body 모듈외').attr('data-ga-event-action', '쇼킹딜_상품').attr('data-ga-event-label', currPrdData.prdNo + '_' + currPrdData.prdNm);
					jQuery("a[name=link]", $targetObj).attr("data-log-body", "{'content_no':'" + currPrdData.prdNo + "', 'content_name':'" + currPrdData.prdNm + "',  'content_type':'PRODUCT'}");
					jQuery("a[name=link]", $targetObj).attr("data-log-index", currPrdIdx + 1);
					jQuery("a[name=link]", $targetObj).attr("click-log-link", currPrdData.clickLogLink);
					
					if(currPrdData.isEmergencyPrd && currPrdData.mktIconImgUrl){
						jQuery('span[name=emergency] > img', $targetObj).attr({'src' : currPrdData.mktIconImgUrl, 'onerror' : currPrdData.onError, 'alt' : '쇼킹딜'});
						jQuery('span[name=emergency]', $targetObj).show();
					}
					jQuery('img[name=image]', $targetObj).attr({'src' : currPrdData.prdImgUrl, 'onerror' : currPrdData.onError, 'alt' : ''});

                    jQuery('div[name=prdNm]', $targetObj).text(currPrdData.prdNm);

                    if(currPrdData.expansInfo) {
                        jQuery('div[name=info]', $targetObj).append('<span class="consult_txt">' + currPrdData.expansInfo.infoText1 + '</span>');
                        jQuery('span[class=consult_txt]', $targetObj).css('color', currPrdData.expansInfo.infoColor1);
                    } else {
                        if (currPrdData.dscRt != '') {
                            jQuery('div[name=info]', $targetObj).append('<span class="sale"><span class="n">' + currPrdData.dscRt + '</span>%</span>');
                        } else {
                            jQuery('div[name=info]', $targetObj).append('<span class="sale sale_tx">쇼킹딜가&nbsp;</span>');
                        }
                        jQuery('div[name=info]', $targetObj).append('<span class="price"><span class="hide">판매가</span><span class="n">' + currPrdData.finalDscPrc + '</span>원' + currPrdData.optTxt + '</span>');
                        if (currPrdData.cmltSelQty != '') {
                            jQuery('div[name=info]', $targetObj).append('<span class="amount"><span class="n">' + currPrdData.cmltSelQty + '</span> 구매</span>');
                        } else {
                            jQuery('div[name=info]', $targetObj).append('<span class="amount">추천상품</span>');
                        }
                    }

                    // PC쇼킹딜 혜택정보 셋팅
                    if(currPrdData.expansInfo) {
                        jQuery('div[name=benefit]', $targetObj).append('<span name="benefit_tx" class="uiq_sale">' + currPrdData.expansInfo.infoText2 + '</span>');
                        jQuery('span[name=benefit_tx]', $targetObj).css('color', currPrdData.expansInfo.infoColor2);
                    } else if(currPrdData.benefitHtml){
                        jQuery('div[name=benefit]', $targetObj).append(currPrdData.benefitHtml);
                    } else {
                        jQuery('div[name=benefit]', $targetObj).append('<span class="uiq_sale">11번가 추천상품</span>');
					}

					if (!currPrdData.couponInfos || currPrdData.couponInfos.length < 1) {
                        jQuery('div[name=coupon]', $targetObj).append('<span class="tx">11번가 추천상품</span>');
                    } else {
						var couponHtml = '<a href="#" class="lk_go" data-log-actionid-area="main_sdeal" data-log-actionid-label="" data-log-body="">' +
											 '<span class="coupon_rate">' +
												 '<span class="coupon_sale"><em></em></span>' +
												 '<span class="coupon_name"></span>' +
											 '</span>' +
										 '</a>';
						var setCouponHtml = function (index, length) {
							var addClassStr = '';
							if (length === 1) {
                                addClassStr = currPrdData.couponInfos[index].cupnIssObjCd === cupnIssObjCdExhibition ? 'full' : 'lt';
							} else {
                                addClassStr = index === 0 ? 'lt' : 'rt';
							}
                            jQuery(couponHtml).clone().appendTo(jQuery('div[name=coupon]', $targetObj))
                                .attr('data-log-body', JSON.stringify(currPrdData.couponInfos[index].logBody))
                                .attr('data-log-actionid-label', currPrdData.couponInfos[index].cupnIssObjCd === cupnIssObjCdExhibition ? 'exhibition' : 'store')
                                .addClass(addClassStr)
                                .attr('href', currPrdData.couponInfos[index].cupnIssObjCd === cupnIssObjCdExhibition ? currPrdData.couponInfos[index].cupnLnkUrl : currPrdData.couponInfos[index].storeLnkUrl)
                                .find('.coupon_name').text(currPrdData.couponInfos[index].cupnNm)
                                .siblings().addClass(currPrdData.couponInfos[index].cupnDscUntVal !== '%' ? 'num_type' : '').find('em').text(currPrdData.couponInfos[index].cupnDscUntVal === '%' ? (currPrdData.couponInfos[index].cupnDscVal + '%') : currPrdData.couponInfos[index].cupnDscValComma);
						}

						if (currPrdData.couponInfos.length === 1) {
                            setCouponHtml(0, currPrdData.couponInfos.length);
                            if (currPrdData.couponInfos[0].cupnIssObjCd === cupnIssObjCdStoreCart || currPrdData.couponInfos[0].cupnIssObjCd === cupnIssObjCdStore) {
                                var storeLinkHtml = '<a href="' + currPrdData.couponInfos[0].storeLnkUrl + '" class="lk_go rt" data-log-actionid-area="main_sdeal" data-log-actionid-label="store" data-log-body="">' +
														'<span class="store">' +
															'<span class="txt">스토어 가기</span>' +
														'</span>' +
													'</a>';
                                jQuery('div[name=coupon]', $targetObj).append(storeLinkHtml).attr('data-log-body', JSON.stringify(currPrdData.couponInfos[0].logBody));
							}
						} else {
                            if ((currPrdData.couponInfos[0].cupnIssObjCd === cupnIssObjCdStoreCart || currPrdData.couponInfos[0].cupnIssObjCd === cupnIssObjCdStore)
								&& (currPrdData.couponInfos[1].cupnIssObjCd === cupnIssObjCdStoreCart || currPrdData.couponInfos[1].cupnIssObjCd === cupnIssObjCdStore)) {
                                var mvLnk = function () {
                                    location.href = currPrdData.couponInfos[0].storeLnkUrl;
                                }
                                jQuery('div[name=coupon]', $targetObj).click(mvLnk);
                            }
                            setCouponHtml(0, currPrdData.couponInfos.length);
                            setCouponHtml(1, currPrdData.couponInfos.length);
                        }
					}

                    window.rakeLog && window.rakeLog.scrollHandler();
                }
            }catch(e){
                jQuery(this).remove();
            }
		}
	};
	var changePage = function(){
		setCurrUlObj();
		setCurrData();
		drawProduct();
		$prdUlArr.hide();
		jQuery($prdUlArr.get(selectedUlIdx)).show();
		setPageing();
	};
	var eventHandler = function(){
		var $shocking_box = jQuery('#shocking_box', $wrapper);
		$shocking_box.bind({
			mouseenter : function(evt){
				stopRolling();
			},
			mouseleave : function(evt){
				startRolling();
			}
		});
		
		var $dealPrdBtn = jQuery('button[name=dealPrdBtn]',$wrapper);
		$dealPrdBtn.bind({
			click : function(evt){
				evt.preventDefault();
				var $thisObj = jQuery(evt.target);
				if(jQuery($thisObj).hasClass('btn btn_prev')){
					selectedUlIdx -- ;
					if(selectedUlIdx < 0 ){
						selectedUlIdx = maxPageIdx;
					}
				}else{
					selectedUlIdx++;
					if(selectedUlIdx > maxPageIdx ){
						selectedUlIdx = 0;
					}
				}
				changePage();
			}
		});
	};
	var setGATaging = function(){
		jQuery( document ).ready(function(){
			try {
				jQuery('a', $wrapper).each(function(idx, obj) {
					jQuery(obj).bind('click', function(){
						catchAnchorGAToEvent();
						if(jQuery(this).attr("click-log-link") != undefined && jQuery(this).attr("click-log-link") != ''){
							jQuery.get(jQuery(this).attr("click-log-link"));
						}
					});
				});
			} catch(e) {
			}
		});
	};
	var rolling = function(){
		selectedUlIdx++;
		if(selectedUlIdx > maxPageIdx ){
			selectedUlIdx = 0;
		}
		changePage();
	};
	var startRolling = function(){
		intervalObj = setInterval(rolling, 5000);
	};

	var stopRolling = function(){
		clearInterval(intervalObj);
		intervalObj = "";
	};
	var initialize = function(){
		setWrapper();
		setData();
		eventHandler();
		setGATaging();
		startRolling();
	};
	
	return {
		init : function(){
			initialize();
		}
	}
})();

Main._2018_11Days =  (function(){
    var $wrapper = {};
    var $shocking_box = {};
    var $shocking_list = {};
    var dispData = {};
    var currData = {};
    var emergencyPrdData = {};
    var emergencyPrdDataSize = 0;
    var emergencyPrdDataIdx = 0;
    var prdData = {};
    var prdDataSize = 0;
    var prdDataIdx = 0;
    var prdDispCnt = 8;
    var $prdUlArr = {};
    var $currPrdUl = {};
    var selectedUlIdx = 0;
    var maxPageIdx = 2;
    var $paging = {};

    var setWrapper = function(){
        $wrapper = jQuery('#main_shocking');
        $shocking_box = jQuery('#shocking_box', $wrapper);
        $shocking_list = jQuery('#shocking_list', $wrapper);
        $prdUlArr = jQuery('ul[name=prdUl]', $shocking_list);
        $paging = jQuery('#paging', $wrapper);
    };
    var setData = function(){
        try {
            jQuery.ajax({
                url : '//www.11st.co.kr/browsing/MainAjax.tmall?method=get11DaysPrdJson',
                type : 'post',
                dataType : 'json',
                success : function (data) {
                    if(data != null || data != '' || typeof(data) != 'undefined'){
                        prdData = data.shockingDealPrdListJson.items;
                        prdDataSize = data.shockingDealPrdListJson.count;
                        if( ((emergencyPrdDataSize + prdDataSize) / 8 ) > 4 ){
                            maxPageIdx = 3;
                        }else{
                            maxPageIdx = parseInt( ((emergencyPrdDataSize + prdDataSize) / 8 ) - 1);
                        }
                        for(var i = 0; i < 32; i++){
                            if( emergencyPrdDataIdx < emergencyPrdDataSize){
                                dispData[i] = emergencyPrdData[emergencyPrdDataIdx];
                                dispData[i].isEmergencyPrd = 'Y';
                                emergencyPrdDataIdx++;
                            }else if( prdDataIdx < prdDataSize){
                                dispData[i] = prdData[prdDataIdx];
                                prdDataIdx++;
                            }
                        }
                        drawPageing();
                        setPageing();
                        setCurrData();
                        setCurrUlObj();
                        drawProduct();
                    }
                }
            });
        } catch(e) {
        }
    };
    var drawPageing = function(){
        for(var i = 0; i <= maxPageIdx; i++ ){
            $paging.append('<button name="pagingBtn" type="button" class="ico">쇼킹딜 상품목록 ' + (i+1) + '</button>');
        }
        var $pagingBtns = jQuery('button[name=pagingBtn]', $paging);
        $pagingBtns.bind({
            click : function(evt){
                var $currObj = jQuery(evt.currentTarget);
                selectedUlIdx = $pagingBtns.index($currObj);
                changePage();
            }
        });
    };var setPageing = function(){
        jQuery('button', $paging).removeClass('on');
        jQuery(jQuery('button', $paging).get(selectedUlIdx)).addClass('on');
    };
    var setCurrData = function(){
        try{
            for(var i = 0; i < prdDispCnt; i++){
                currData[i] = dispData[( (prdDispCnt * selectedUlIdx) + i )];
            }
        }catch(e){
        }
    };
    var setCurrUlObj = function(){
        $currPrdUl = jQuery($prdUlArr.get(selectedUlIdx));
    };
    var drawProduct = function(){
        var $currPrdObjArr =jQuery('li[name=product]', $currPrdUl );
        var startIdx = 0;
        for(var loopIdx=0; loopIdx<prdDispCnt; loopIdx++){
            try{
                var currPrdIdx = startIdx + loopIdx;
                var currPrdData = currData[currPrdIdx];
                var $targetObj = jQuery($currPrdObjArr.get(currPrdIdx));


                if(jQuery('a[name=link]', $targetObj).attr('href') == '#'){

                    jQuery('a[name=link]', $targetObj).attr('href', currPrdData.url1);
                    jQuery('a[name=link]', $targetObj).attr('data-ga-event-category', 'PC메인_body 모듈외').attr('data-ga-event-action', '십일절_상품').attr('data-ga-event-label', currPrdData.prdNo + '_' + currPrdData.prdNm);
                    jQuery("a[name=link]", $targetObj).attr("data-log-body", "{'content_no':'" + currPrdData.prdNo + "', 'content_name':'" + currPrdData.prdNm + "', 'last_discount_price':'" + currPrdData.finalDscPrc + "',  'content_type':'PRODUCT'}");
                    jQuery("a[name=link]", $targetObj).attr("data-log-index", currPrdIdx + 1);
                    jQuery("a[name=link]", $targetObj).attr("click-log-link", currPrdData.clickLogLink);

                    if(currPrdData.isEmergencyPrd && currPrdData.mktIconImgUrl){
                        jQuery('span[name=emergency] > img', $targetObj).attr({'src' : currPrdData.mktIconImgUrl, 'onerror' : currPrdData.onError, 'alt' : '십일절'});
                        jQuery('span[name=emergency]', $targetObj).show();
                    }
                    jQuery('img[name=image]', $targetObj).attr({'src' : currPrdData.prdImgUrl, 'onerror' : currPrdData.onError, 'alt' : ''});
					
                    jQuery('div[name=prdNm]', $targetObj).text(currPrdData.prdNm);

                    if(currPrdData.dscRt != ''){
                    	if( currPrdData.dscRt >= 1){
                            jQuery('div[name=info]', $targetObj).append('<span class="sale"><span class="n">'+ currPrdData.dscRt +'</span>%</span>');
						}
                    }
                    jQuery('div[name=info]', $targetObj).append('<span class="price"><span class="hide">판매가</span><span class="n">'+ currPrdData.finalDscPrc +'</span>원'+ currPrdData.optTxt +'</span>');
                    if(currPrdData.cmltSelQty != ''){
                        jQuery('div[name=info]', $targetObj).append('<span class="amount"><span class="n">'+ currPrdData.cmltSelQty +'</span> 구매</span>');
                    }else{
                        jQuery('div[name=info]', $targetObj).append('<span class="amount">추천상품</span>');
                    }

                    if(currPrdData.flag1){
                        jQuery('div[name=benefit]', $targetObj).append('<span class="tx"><strong class="point">'+ currPrdData.flag1 +'</strong></span>');
                    }
                    if(currPrdData.flag2){
                        jQuery('div[name=benefit]', $targetObj).append('<span class="tx">'+ currPrdData.flag2 +' <strong class="point">'+ currPrdData.flag3 +'</strong></span>');
                    }
                    if(currPrdData.flag4){
                        jQuery('div[name=benefit]', $targetObj).append('<span class="tx">'+ currPrdData.flag4 +' <strong class="point">'+ currPrdData.flag5 +'</strong></span>');
                    }
                    if(currPrdData.flag6){
                        jQuery('div[name=benefit]', $targetObj).append('<span class="tx">'+ currPrdData.flag6 +' <strong class="point">'+ currPrdData.flag7 +'</strong></span>');
                    }
                    if(!currPrdData.flag1 && !currPrdData.flag2 && !currPrdData.flag4 && !currPrdData.flag6 ){
                        var recommendText = '';
                        if(currPrdData.isDealPrd){
                            recommendText = '쇼킹딜 추천상품';
                        }else{
                            recommendText = '11번가 추천상품';
                        }
                        jQuery('div[name=benefit]', $targetObj).append('<span class="tx">'+ recommendText +'</span>');
                    }
                    if( currPrdData.soldOut == 'Y'){
                        jQuery('div.product_type1', $targetObj).addClass('closed');
                        jQuery('div[name=info]', $targetObj).append('<p class="txt_closed">상품이 품절되었습니다.</p>');
                        jQuery('a[name=link]', $targetObj).removeAttr('href');
                        jQuery('span.line_frame', $targetObj).append('<span class="txt">Sold Out</span>');
                    }

                    window.rakeLog && window.rakeLog.scrollHandler();
                }
            }catch(e){
                jQuery(this).remove();
            }
        }
    };
    var changePage = function(){
        setCurrUlObj();
        setCurrData();
        drawProduct();
        $prdUlArr.hide();
        jQuery($prdUlArr.get(selectedUlIdx)).show();
        setPageing();
    };
    var eventHandler = function(){
        var $shocking_box = jQuery('#shocking_box', $wrapper);
        $shocking_box.bind({
            mouseenter : function(evt){
                stopRolling();
            },
            mouseleave : function(evt){
                startRolling();
            }
        });

        var $dealPrdBtn = jQuery('button[name=dealPrdBtn]',$wrapper);
        $dealPrdBtn.bind({
            click : function(evt){
                evt.preventDefault();
                var $thisObj = jQuery(evt.target);
                if(jQuery($thisObj).hasClass('btn btn_prev')){
                    selectedUlIdx -- ;
                    if(selectedUlIdx < 0 ){
                        selectedUlIdx = maxPageIdx;
                    }
                }else{
                    selectedUlIdx++;
                    if(selectedUlIdx > maxPageIdx ){
                        selectedUlIdx = 0;
                    }
                }
                changePage();
            }
        });
    };
    var setGATaging = function(){
        jQuery( document ).ready(function(){
            try {
                jQuery('a', $wrapper).each(function(idx, obj) {
                    jQuery(obj).bind('click', function(){
                        catchAnchorGAToEvent();
                        if(jQuery(this).attr("click-log-link") != undefined && jQuery(this).attr("click-log-link") != ''){
                            jQuery.get(jQuery(this).attr("click-log-link"));
                        }
                    });
                });
            } catch(e) {
            }
        });
    };
    var rolling = function(){
        selectedUlIdx++;
        if(selectedUlIdx > maxPageIdx ){
            selectedUlIdx = 0;
        }
        changePage();
    };
    var startRolling = function(){
        intervalObj = setInterval(rolling, 5000);
    };

    var stopRolling = function(){
        clearInterval(intervalObj);
        intervalObj = "";
    };
    var initialize = function(){
        setWrapper();
        setData();
        eventHandler();
        setGATaging();
        startRolling();
    };

    return {
        init : function(){
            initialize();
        }
    }
})();

//오늘의 찬스
Main.todayChance = (function(){
	var $wrapper = {};
	var data = {};
	var dataSize = 0;
	var showCount = 6;
	var startIdx = 0;
	var $bnrObjArr = {};
	var $infoBtnObj = {};
	var $nextBtnObj = {};
	var $prevBtnObj = {};
	var randomIndex = 0;
	var _stckImg6 = [];

	var setData = function(){
		data = eval("TodayChanceAD");
		dataSize = data.totalCount>=12?12:data.totalCount;
	};
	var setWrapper = function(){
		$wrapper = jQuery("#todayChanseWrap");
	};
	var setBnrObj = function(){
		$bnrObjArr = jQuery("li[name=todayChanceBnr]", $wrapper);
	};
	var setBtnObj = function(){
		$infoBtnObj = jQuery("button[name=infoBtn]", $wrapper);
		$nextBtnObj = jQuery("button[name=todayChanceNext]", $wrapper);
		$prevBtnObj = jQuery("button[name=todayChancePrev]", $wrapper);
	};
	var setStartIdx = function(){
		var arrWeight = {};
		arrWeight[0] = 10;
		arrWeight[1] = 10;
		arrWeight[2] = 10;
		arrWeight[3] = 10;
		arrWeight[4] = 10;
		arrWeight[5] = 6;
		arrWeight[6] = 6;
		arrWeight[7] = 6;
		arrWeight[8] = 6;
		arrWeight[9] = 6;
		arrWeight[10] = 4;
		arrWeight[11] = 4;
		randomIndex = getRanNumWeight(arrWeight);
		startIdx = Math.floor(randomIndex / showCount) * showCount;
	};
	var setGATaging = function(){
		jQuery( document ).ready(function(){
			try {
				var as = jQuery("#todayChanseWrap a");
				for(var i=0,len=as.length; i<len; i++){
					as[i].bind('click', catchAnchorGAToEvent );
				}
			} catch(e) {
			}
		});
	};
	var initialize = function(){
		setWrapper();
		setBnrObj();
		setBtnObj();
		setData();
		setStartIdx();
		drawBanner();
		eventHandler();
		
		setGATaging();
	};
	var drawBanner = function(){
		try{
			var liIdx = 0;
			var strTRCNO = "";
			for(var i=startIdx; i<startIdx + showCount; i++){
				var currData = data.DATA[i];
				jQuery("img[name=todayChanceImage]", $bnrObjArr[liIdx]).attr({"src": currData.IMG1,"alt" : currData.TXT1});
				jQuery("a[name=todayChanceLink]", $bnrObjArr[liIdx]).attr("href", "javascript:"+currData.JURL1).attr("data-ga-event-category", "PC메인_body 모듈외").attr("data-ga-event-action", "오늘의_찬스_상품").attr("data-ga-event-label", currData.NUM1 + '_' + currData.TXT1);
				jQuery("a[name=todayChanceLink]", $bnrObjArr[liIdx]).attr("data-log-body", "{'content_no':'" + currData.NUM1 + "', 'content_name':'" + currData.TXT1 + "', 'content_type':'PRODUCT', 'product_price':'" + currData.PRC1 + "', 'link_url':'http://www.11st.co.kr/product/SellerProductDetail.tmall?method=getSellerProductDetail&prdNo=" + currData.NUM1 + "'}");
				jQuery("a[name=todayChanceLink]", $bnrObjArr[liIdx]).attr("data-log-index", i + 1);
				jQuery("div[name=todayChanceTitle]",  $bnrObjArr[liIdx]).html(currData.TXT1);
				//jQuery("span[name=todayChancePrice]",  $bnrObjArr[liIdx]).html(currData.PRC1);
				//jQuery("span[name=optTxt]",  $bnrObjArr[liIdx]).html(currData.OPT_TXT);

                if(currData.expansInfo) {
                    jQuery('div[class=info]', $bnrObjArr[liIdx]).html('<span class="consult_txt">' + currData.expansInfo.infoText1 + '</span>');
                    jQuery('span[class=consult_txt]', $bnrObjArr[liIdx]).css('color', currData.expansInfo.infoColor1);
                } else {
                    jQuery('div[class=info]', $bnrObjArr[liIdx]).html('<span class="price"><span class="hide">판매가</span><span name="todayChancePrice" class="n">'+ currData.PRC1 +'</span>원 <span name="optTxt">'+ currData.OPT_TXT +'</span>');
                }

				if(currData.TRCNO1 != "-1"){
					var trcGubn = $bnrObjArr.attr("title");
					if(trcGubn != null & currData.TRCNO1 > 0) {
						strTRCNO += trcGubn + currData.TRCNO1 + "^";
						var img = new Image();
						_stckImg6.push(img);
					}
				}
				liIdx++;
			}
			img.src =  '//st.11st.co.kr/a.st?a='+strTRCNO;
		}catch(e){
		}
	};
	var eventHandler = function(){
		$infoBtnObj.bind({
			click : function(){
				jQuery(".help_pop", $wrapper).toggle('display');
			}
		});
		
		$nextBtnObj.bind({
			click : function(){
				startIdx = startIdx + showCount;
				if(startIdx > dataSize){
					startIdx = 1;
				}else if(startIdx == dataSize){
					startIdx = 0;
				}
				drawBanner();
			}
		});

		$prevBtnObj.bind({
			click : function(){
				startIdx = startIdx - showCount;
				if(startIdx < 0){
					startIdx = dataSize-showCount;
				}
				drawBanner();
			}
		});
	};
	return {
		init : function(){
			initialize();
		}
	}
})();

//메인 파트너스 영역
Main.partners = function() {
	var $wrapper = {};
	var data = {};
	var dataSize = 0;
	var curPageIdx = 0 ;
	var maxPageIdx = 0 ;
	var showCnt = 0 ;
	var option = {};

	var setData = function(){
		//set Initial Data
		data = option.jsonData.DATA;
		dataSize = option.jsonData.totalCount;
		$wrapper = jQuery('#'+ option.wrapper);

		// set Operating Data
		maxPageIdx = Math.floor(dataSize / showCnt) ;
		if(maxPageIdx <= 1){
			jQuery('.btnctr_pn',$wrapper).hide();
		}

		//set paging Elements
		jQuery('.in_prev',$wrapper).before('<em>'+curPageIdx+'<\/em>/'+maxPageIdx);

	};
	
	var setGATaging = function(){
		jQuery( document ).ready(function(){
			try {
				jQuery("#main_brandAd a").each(function(idx, obj) {
					jQuery(obj).bind('click',catchAnchorGAToEvent);
				});
			} catch(e) {
			}
			try {
				jQuery("#main_direct_service a").each(function(idx, obj) {
					jQuery(obj).bind('click',catchAnchorGAToEvent);
				});
			} catch(e) {
			}
		});
	};
	
	var initialize = function(){
		setData();
		drawBanner();
		eventHandler();
		setGATaging();
	};

	var drawBanner = function(){
		try{
			jQuery('ul' , $wrapper ).hide();
			var $currUL = jQuery('#main_ul_' +option.wrapper+'_'+curPageIdx);
			if($currUL.length < 1){
				jQuery('h1', $wrapper ).after('<ul id="main_ul_' +option.wrapper+'_'+curPageIdx+'">');
				$currUL =jQuery('#main_ul_' +option.wrapper+'_'+curPageIdx);

				var startIdx =  curPageIdx * showCnt ;
				var endIdx = startIdx + showCnt ;
				for( i = startIdx ; i < endIdx ; i++){
					var element = '<li>';
					element += '<a href="'+ data[i].URL1 +'" onclick="doCommonStat(\'MAINMB130101\');"  data-ga-event-category="PC메인_body 모듈외" data-ga-event-action="주요파트너몰_배너" data-ga-event-label="' + data[i].TXT1 + '" data-log-body="{\'btn_name\':\'' + data[i].TXT1 + '\'}" data-log-index="' + (i+1) + '">';
					element += '	<img src="'+data[i].IMG1+'" onError="javascript:this.src=\'' +_IMG_URL_+ '/img/prd_size/noimg_100.gif\';" alt="'+getAltTxt(data[i].ETXT1,data[i].TXT1)+'" >';
					element += '<\/a>';
					element += '<\/li>';
					$currUL.append(element);
				}
			}else{
				$currUL.show();
			}

			jQuery('.btnctr_pn' , $wrapper ).find('em').html(curPageIdx+1);
		}catch(e){
		}
	};

	var eventHandler = function(){
		jQuery('.in_prev', $wrapper).bind({
			click : function(evt){
				curPageIdx--;
				if(curPageIdx < 0){
					curPageIdx = maxPageIdx - 1;
				}
				drawBanner();
			}
		});
		jQuery('.in_next', $wrapper).bind({
			click : function(){
				curPageIdx++;
				if(curPageIdx >= maxPageIdx){
					curPageIdx = 0;
				}
				drawBanner();
			}
		});
	};

	var hideAll = function () {
		jQuery('#'+option.wrapper).hide();
	}

	return {
		init : function(obj){
			option = obj;
			if(option.minShowCount <= option.jsonData.totalCount){
				if(option.maxShowCount <= option.jsonData.totalCount){
					showCnt = option.maxShowCount;
				}else{
					showCnt = option.minShowCount;
				}
				initialize();
			}else{
				hideAll();
			}
		}
	};
};

Main.recommendPrd =  function(){
	var $wrapper = {};
	var dispData = {};
	var currData = {};
	var prdData = {};
	var prdDataSize = 0;
	var prdDataIdx = 0;
	var prdDispCnt = 6;
	var $prdUlArr = {};
	var $currPrdUl = {};
	var selectedUlIdx = 0;
	var maxPageIdx = 2;
	var $paging = {};
	var title = '';
	
	var setWrapper = function(id){
		$wrapper = jQuery('#'+id);
		$prdUlArr = jQuery('ul[name=prdUl]', $wrapper);
		$paging = jQuery('#paging', $wrapper);
	};
	var setData = function(id){
		var dispMaxCnt = 200;
		var url = '//www.11st.co.kr/browsing/MainAjax.tmall?method=get' + id + '&dispMaxCnt=' + dispMaxCnt;
		try {
			jQuery.ajax({
			    url : url,
			    type : 'post',
			    dataType : 'json',
			    success : function (data) {
			    	if(data != null || data != '' || data != {} || typeof(data) != 'undefined'){
			    		if('UserBasketRecopickPrd' == id){
			    			if(data.recopickProductList){
			    				prdData = data.recopickProductList;
			    				prdDataSize = data.recopickProductList.length;
			    				title = data.recopickTitle;
			    				$('#title','#'+id).html(title);
			    			}else{
			    				jQuery($wrapper).hide();
			    			}
			    		}else if('UserActionBasedRecommendCtgrPrd' == id){
			    			if(data.recommendCategoryProduct){
			    				prdData = data.recommendCategoryProduct.items;
			    				prdDataSize = data.recommendCategoryProduct.items.length;
			    				title = data.recommendCategoryProduct.title;
			    				$('#title','#'+id).html('지금 뜨는 <strong class="hashtag">' + data.recommendCategoryProduct.extraText + '</strong> 상품');
			    			}else{
			    				jQuery($wrapper).hide();
			    			}
			    		}
						if( (prdDataSize / 6 ) > 4 ){
							maxPageIdx = 3;
						}else{
							maxPageIdx = parseInt( (prdDataSize / 6 ) - 1);
						}
						dispData = prdData;
						
						jQuery($prdUlArr.get(selectedUlIdx)).show();
						setCurrData();
						setCurrUlObj();
						drawProduct();
					}
			    }
			});
		} catch(e) {
		}
	};
	var setCurrData = function(){
		try{
			for(var i = 0; i < prdDispCnt; i++){
				currData[i] = dispData[( (prdDispCnt * selectedUlIdx) + i )];
			}
		}catch(e){
		}
	};
	var setCurrUlObj = function(){
		$currPrdUl = jQuery($prdUlArr.get(selectedUlIdx), $wrapper);
	};
	var drawProduct = function(){
		var $currPrdObjArr =jQuery('li[name=product]', $currPrdUl );
		var startIdx = 0;
		var dataLogArea = '';
		for(var loopIdx=0; loopIdx<prdDispCnt; loopIdx++){
			try{
				var currPrdIdx = startIdx + loopIdx;
				var currPrdData = currData[currPrdIdx];
				var $targetObj = jQuery($currPrdObjArr.get(currPrdIdx));

				if(jQuery('a[name=link]', $targetObj).attr('href') == '#'){
					jQuery('a[name=link]', $targetObj).attr('href', currPrdData.link).attr('onclick', "rakeLog.sendRakeLog(this);");
					dataLogArea = currPrdData.logData.area;
					jQuery("a[name=link]", $targetObj).attr('data-log-actionid-area', dataLogArea);
					jQuery("a[name=link]", $targetObj).attr('data-log-actionid-label', currPrdData.logData.label);
					var dataLogBody = JSON.stringify(currPrdData.logData.dataBody);
					jQuery("a[name=link]", $targetObj).attr("data-log-body", dataLogBody.replace(/"/g, "'"));
					
					jQuery('img[name=image]', $targetObj).attr({'src' : currPrdData.imgUrl, 'onerror' : 'javascript:this.src=\'' +_IMG_URL_+ '/img/prd_size/noimg_120.gif\';', 'alt' : currPrdData.prdNm});
					
					jQuery('div[name=prdNm]', $targetObj).text(currPrdData.prdNm);
					
					jQuery('div[name=info]', $targetObj).append('<span class="price"><span class="hide">판매가</span><span class="n">'+ currPrdData.finalDscPrc +'</span>원'+ currPrdData.optPrcText +'</span>');
				}
			}catch(e){
				jQuery(this).remove();
			}
		}
		jQuery('button[name=recommPrdBtn]',$wrapper).attr('data-log-actionid-area', dataLogArea);
	};
	var changePage = function(){
		setCurrUlObj();
		setCurrData();
		drawProduct();
		$prdUlArr.hide();
		jQuery($prdUlArr.get(selectedUlIdx)).show();
	};
	var eventHandler = function(){
		var $dealPrdBtn = jQuery('button[name=recommPrdBtn]',$wrapper);
		$dealPrdBtn.bind({
			click : function(evt){
				evt.preventDefault();
				var $thisObj = jQuery(evt.target);
				if(jQuery($thisObj).hasClass('btn btn_prev')){
					selectedUlIdx -- ;
					if(selectedUlIdx < 0 ){
						selectedUlIdx = maxPageIdx;
					}
				}else{
					selectedUlIdx++;
					if(selectedUlIdx > maxPageIdx ){
						selectedUlIdx = 0;
					}
				}
				changePage();
			}
		});
	};
	var initialize = function(id){
		setWrapper(id);
		setData(id);
		eventHandler();
	};
	
	return {
		init : function(id){
			initialize(id);
		}
	};
};
